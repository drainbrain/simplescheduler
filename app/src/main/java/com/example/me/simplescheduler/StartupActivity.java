package com.example.me.simplescheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

import static com.example.me.simplescheduler.AlarmReceiver.sendDebugMsg;
import static com.example.me.simplescheduler.MainActivity.calcNextCustomTime;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_ID;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NAME;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NUM;
import static com.example.me.simplescheduler.RemindersStorage.NTFDATETIME;
import static com.example.me.simplescheduler.RemindersStorage.STATE;

public class StartupActivity extends BroadcastReceiver {
    RemindersStorage myDb;
    private Calendar calNotificationDate;
    String ntfDesc="";
    String nextNotificationTimeInMillis ="";
    int reqCode=0;
    String secondaryRepeatFreq;
    String primaryRepeatFreq;
    String strState;
    private String auxTimeInfo;
    private String notificationType;
    private boolean bPostBoot;
    Context ctx;
    void cancelReminder(int iReqCode){
        Intent cancelIntent = new Intent(ctx, AlarmReceiver.class);//the same as up
        PendingIntent pendingIntent = PendingIntent.getBroadcast(ctx,
                iReqCode, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);//the same as up
        AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);//important
        pendingIntent.cancel();//important

    }
    public void scheduleNotification(int reqID) {
        //data to pass to alert handler
        Intent notifyIntent = new Intent(ctx, AlarmReceiver.class);
        notifyIntent.putExtra(NOTIFICATION_ID, "" + reqID);
        notifyIntent.putExtra(NOTIFICATION_NUM, "Primary Notification");
        notifyIntent.putExtra(NOTIFICATION_NAME, ntfDesc);

        //start scheduler
        PendingIntent broadcast = PendingIntent.getBroadcast(ctx, reqCode, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);


        Calendar calNextNotificationTime = Calendar.getInstance();
        calNextNotificationTime.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));
        Calendar calCurrentTime = Calendar.getInstance();
        if(!primaryRepeatFreq.equalsIgnoreCase("NOTIFY ONCE")) {
            //has one or more scheduled notifications been missed
            if( System.currentTimeMillis() > Long.parseLong(nextNotificationTimeInMillis)) {
                // set the Calendar obj to last scheduled notification
                int dow = calNextNotificationTime.get(Calendar.DAY_OF_WEEK);
                int dom = calNextNotificationTime.get(Calendar.DAY_OF_MONTH);
                int doy = calNextNotificationTime.get(Calendar.DAY_OF_YEAR);
                if (primaryRepeatFreq.equalsIgnoreCase("DAILY")) {
                    //see if it makes it before current time and if so makle it next day
                    if (calCurrentTime.after(calNextNotificationTime))
                        calNextNotificationTime.add(Calendar.DAY_OF_YEAR, 1);
                }
                else if (primaryRepeatFreq.equalsIgnoreCase("WEEKLY")) {
                    //give it the day of week the user set for this reminder
                    calNextNotificationTime.set(Calendar.DAY_OF_WEEK, dow);
                    //see if it makes it before current time and if so make it next week
                    if( calCurrentTime.after(calNextNotificationTime) )
                        calNextNotificationTime.add(Calendar.WEEK_OF_YEAR, 1);
                }
                else if (primaryRepeatFreq.equalsIgnoreCase("MONTHLY")) {
                    //give it the day of week the user set for this reminder
                    calNextNotificationTime.set(Calendar.DAY_OF_WEEK, dow);
                    //see if it makes it before current time and if so make it next month
                    if( calCurrentTime.after(calNextNotificationTime) )
                        calNextNotificationTime.add(Calendar.MONTH, 1);
                }
                else if (primaryRepeatFreq.equalsIgnoreCase("YEARLY")) {
                    //see if it makes it before current time and if so make it next year
                    if( calCurrentTime.after(calNextNotificationTime) )
                        calNextNotificationTime.add(Calendar.YEAR, 1);
                }
                else if (primaryRepeatFreq.equalsIgnoreCase("CUSTOM")) {
                    long lNextNotificationTimeInMillis = Long.parseLong(nextNotificationTimeInMillis);
                    lNextNotificationTimeInMillis = calcNextCustomTime(lNextNotificationTimeInMillis, auxTimeInfo);
                    calNextNotificationTime.setTimeInMillis(lNextNotificationTimeInMillis);
                    //see if next notification time is still behind current time
                    if( calCurrentTime.after(calNextNotificationTime) ){
                        //set it to current time
                        calNextNotificationTime = Calendar.getInstance();
                        lNextNotificationTimeInMillis = calNextNotificationTime.getTimeInMillis();
                        //add the custom interval to it
                        lNextNotificationTimeInMillis = calcNextCustomTime(lNextNotificationTimeInMillis, auxTimeInfo);
                        calNextNotificationTime.setTimeInMillis(lNextNotificationTimeInMillis);
                    }
                }
            }

            AlarmReceiver.debugMesg("re-scheduled repeating primary primary notification of \"" + ntfDesc + "\" for " +
                    ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()));
            sendDebugMsg(ctx, "StartupActivity: re-scheduled repeating primary notification of \"" + ntfDesc + "\" for " +
                    ReminderScheduler.logDateFormat.format(calNextNotificationTime.getTime()) + ". request code = " + reqCode);
        }
        else{
            AlarmReceiver.debugMesg("re-scheduled primary notification of \"" + ntfDesc + "\" for " +
                    ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()));
            sendDebugMsg(ctx, "StartupActivity: re-scheduled primary notification of \"" + ntfDesc + "\" for " +
                    ReminderScheduler.logDateFormat.format(calNextNotificationTime.getTime()) + ". request code = " + reqCode);
        }
        nextNotificationTimeInMillis = "" + calNextNotificationTime.getTimeInMillis();

        //uncomment for debugging
/*
        String sCurrTime = dateFormat.format(calCurrentTime.getTime());
        String sNextNotificationTime = dateFormat.format(calNextNotificationTime.getTime());
*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calNextNotificationTime.getTimeInMillis(), broadcast);
        else
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calNextNotificationTime.getTimeInMillis(), broadcast);

        //add new entry into database
        updateDB(STATE, "ON", "" + reqCode );
        updateDB(NTFDATETIME, "" + nextNotificationTimeInMillis, "" + reqCode );

    }

    void updateDB(String strDBfield, String val, String strReqCode){
        myDb = RemindersStorage.getInstance(ctx);
        SQLiteDatabase db = myDb.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(strDBfield, val ); //These Fields should be your String values of actual column names
        db.update(RemindersStorage.TABLE_NAME, cv, " request_code = ?", new String[]{strReqCode});
        myDb.close();
    }

    void scheduleReminders(){
        boolean bExpired_ = false;
        Intent intent = new Intent(ctx, AlarmReceiver.class);//the same as up
        boolean isSet = (PendingIntent.getBroadcast(ctx, reqCode, intent, PendingIntent.FLAG_NO_CREATE) != null);

        if(primaryRepeatFreq.equalsIgnoreCase("notify once") && System.currentTimeMillis() > Long.parseLong(nextNotificationTimeInMillis)){
            bExpired_ = true;
        }

        Calendar calNextNotificationTime = Calendar.getInstance();
        calNextNotificationTime.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));

        sendDebugMsg(ctx, "StartupActivity: Displaying Reminder \"" + ntfDesc + "\", req code " + reqCode +
                ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()));

        if(bExpired_){
            //see if alarm associated with reqID is set
            if (isSet) {
                sendDebugMsg(ctx,"StartupActivity: Reminder \"" + ntfDesc + "\", req code " + reqCode +
                        ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
                        " is expired and will be canceled.");
                cancelReminder(reqCode);
            }
            if(strState.equalsIgnoreCase("ON")){
                sendDebugMsg(ctx,"StartupActivity: Reminder \"" + ntfDesc + "\", req code " + reqCode +
                        ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
                        " is expired.");
                updateDB(STATE, "OFF", "" + reqCode);
            }
            else{
                sendDebugMsg(ctx, "StartupActivity: Reminder \"" + ntfDesc + "\", req code " + reqCode +
                        ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
                        " is expired and not currently set.");
            }
        }
        else{
            sendDebugMsg(ctx,"StartupActivity: Reminder \"" + ntfDesc + "\", req code " + reqCode +
                    ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
                    " is not expired.");
            if(strState.equalsIgnoreCase("ON")) {
                if(!isSet){
                    sendDebugMsg(ctx,"Reminder \"" + ntfDesc + "\", req code " + reqCode +
                            " is not scheduled. Needs to be rescheduled.");
                    scheduleNotification(reqCode);
                }
            }
        }
    }

    void showReminders(){
        myDb = RemindersStorage.getInstance(ctx);
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            //showMessage("Alert","No reminders to show");
        }
        else {

            //StringBuffer buffer = new StringBuffer();
            while (res.moveToNext()) {
                //get the data
                ntfDesc = res.getString(RemindersStorage.FIELDS.NAME);
                nextNotificationTimeInMillis = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
                reqCode = res.getInt(RemindersStorage.FIELDS.REQCODE);
                primaryRepeatFreq = res.getString(RemindersStorage.FIELDS.PRIMARYREMINDERFREQ);
                secondaryRepeatFreq = res.getString(RemindersStorage.FIELDS.SECONDARYREMINDERFREQ);
                strState = res.getString(RemindersStorage.FIELDS.STATE);
                auxTimeInfo = res.getString(RemindersStorage.FIELDS.AUXTIMEINFO);
                notificationType = res.getString(RemindersStorage.FIELDS.NOTIFICATIONTYPE);
                Toast.makeText(ctx, "scheduling " + ntfDesc, Toast.LENGTH_LONG).show();
                scheduleReminders();
            }
            // Show all data
            // showMessage("Data", buffer.toString());
        }
        myDb.close();
    }




    @Override
    public void onReceive(Context context, Intent intent) {
        ctx = context;
        Toast.makeText(context, "my shit booting!!!!!!!!!", Toast.LENGTH_SHORT).show();
        showReminders();
/*
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(ReminderScheduler.NOTIFICATION_NUM, 1);
            context.startActivity(i);
        }
*/
    }
}
