package com.example.me.simplescheduler;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity {

    PackageInfo pInfo;
    private int getVersion() {
        int version = -1;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e(this.getClass().getSimpleName(), "Name not found", e1);
        }
        return version;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        getVersion();
        TextView versionTV = (TextView) findViewById(R.id.versionString);
        versionTV.setText(pInfo.versionName + "." + pInfo.versionCode);

    }
}
