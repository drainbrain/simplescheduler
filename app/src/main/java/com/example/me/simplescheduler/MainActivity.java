package com.example.me.simplescheduler;

import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import static android.graphics.Typeface.*;
import static com.example.me.simplescheduler.AlarmReceiver.fname;
import static com.example.me.simplescheduler.AlarmReceiver.debugMesg;
import static com.example.me.simplescheduler.AlarmReceiver.sendDebugMsg;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_ID;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NAME;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NUM;
import static com.example.me.simplescheduler.ReminderScheduler.delay;
import static com.example.me.simplescheduler.ReminderScheduler.iGlobalRequestCode;
import static com.example.me.simplescheduler.RemindersStorage.NTFDATETIME;
import static com.example.me.simplescheduler.RemindersStorage.STATE;
import static com.example.me.simplescheduler.RemindersStorage.TABLE_NAME;

/**
 * Created by me on 2/8/2018.
 */
class Ntf {
  String name;
  int reqID;
}


public class MainActivity extends AppCompatActivity {
  private Calendar calNotificationDate;
  List<Ntf> ntfList = new LinkedList<>();
  RemindersStorage myDb;
  boolean bPause = false;
  String ntfDesc = "";
  String nextNotificationTimeInMillis = "";
  int reqCode = 0;
  String secondaryRepeatFreq;
  String primaryRepeatFreq;
  String strState;
  private ActionMode mActionMode;
  private int iSelectedRowIdx;
  private String auxTimeInfo;
  private String notificationType;
  private boolean bPostBoot;
  static boolean writeToFile = false;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    writeToFile = false;


    Log.d("MainActivity", "here");
    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !notificationManager.isNotificationPolicyAccessGranted()) {
      Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
      startActivity(intent);
    }
    //String sss = NotificationActivity.getFilePath(NotificationActivity.filename, this);
    //deleteDatabase(RemindersStorage.DATABASE_NAME);
    //1st get the intent data to determine where notification came from
    //sendDebugMsg(this, "MainActivity: dataBundle=null");
    setContentView(R.layout.activity_main);
    File fl = new File(fname);
    if (fl.exists())
      fl.delete();
    debugMesg("starting");

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    //toolbar.setTitle("MOFO");
    setSupportActionBar(toolbar);

    int rid = getStartingReqcode();
    if (rid > -1)
      iGlobalRequestCode = rid + 1;
    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent scheduleReminder = new Intent(MainActivity.this, ReminderScheduler.class);
        //startActivity(scheduleReminder);
        startActivityForResult(scheduleReminder, 2);
      }
    });
    showReminders();
  }

  public int getStartingReqcode() {
    myDb = RemindersStorage.getInstance(this);
    SQLiteDatabase db = myDb.getWritableDatabase();
    Cursor res = db.rawQuery("SELECT MAX(request_code) FROM " + TABLE_NAME, null);
    int id;
    if (res != null) {
      res.moveToFirst();
      id = res.getInt(0);
    }
    else
      id = -1;

    myDb.close();
    return id;
  }

  void updateDB(String strDBfield, String val, String strReqCode) {
    myDb = RemindersStorage.getInstance(MainActivity.this);
    SQLiteDatabase db = myDb.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(strDBfield, val); //These Fields should be your String values of actual column names
    db.update(RemindersStorage.TABLE_NAME, cv, " request_code = ?", new String[]{strReqCode});
    myDb.close();
  }

  void cancelReminder(int iReqCode) {
    Intent cancelIntent = new Intent(MainActivity.this, AlarmReceiver.class);//the same as up
    PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this,
      iReqCode, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);//the same as up
    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    alarmManager.cancel(pendingIntent);//important
    pendingIntent.cancel();//important

  }

  static long calcNextCustomTime(long baseTime, String auxTimeInfo) {
    String[] auxDataParts = auxTimeInfo.split(":");
    int amount = Integer.parseInt(auxDataParts[1]);
    long lNextNotificationTimeInMillis = baseTime;
    if (auxDataParts[0].equalsIgnoreCase("seconds")) {
      lNextNotificationTimeInMillis += amount * 1000;
    }
    else if (auxDataParts[0].equalsIgnoreCase("minutes")) {
      lNextNotificationTimeInMillis += amount * 60 * 1000;
    }
    else if (auxDataParts[0].equalsIgnoreCase("hours")) {
      lNextNotificationTimeInMillis += amount * 60 * 60 * 1000;
    }
    else if (auxDataParts[0].equalsIgnoreCase("days")) {
      lNextNotificationTimeInMillis += amount * 60 * 60 * 24 * 1000;
    }

    return lNextNotificationTimeInMillis;
  }


  public void scheduleNotification(int reqID) {

    calNotificationDate = Calendar.getInstance();
    calNotificationDate.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));

    //data to pass to alert handler
    Intent notifyIntent = new Intent(this, AlarmReceiver.class);
    notifyIntent.putExtra(NOTIFICATION_ID, "" + reqID);
    notifyIntent.putExtra(NOTIFICATION_NUM, "Primary Notification");
    notifyIntent.putExtra(NOTIFICATION_NAME, ntfDesc);

    //start scheduler
    PendingIntent broadcast = PendingIntent.getBroadcast(this, reqCode, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);


    Calendar calNextNotificationTime = Calendar.getInstance();
    Calendar calCurrentTime = Calendar.getInstance();
    if (!primaryRepeatFreq.equalsIgnoreCase("NOTIFY ONCE")) {
      //has one or more scheduled notifications been missed
      if (System.currentTimeMillis() > Long.parseLong(nextNotificationTimeInMillis)) {
        // set the Calendar obj to last scheduled notification
        calNextNotificationTime.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));
        int dow = calNextNotificationTime.get(Calendar.DAY_OF_WEEK);
        int dom = calNextNotificationTime.get(Calendar.DAY_OF_MONTH);
        int doy = calNextNotificationTime.get(Calendar.DAY_OF_YEAR);
        if (primaryRepeatFreq.equalsIgnoreCase("DAILY")) {
          //see if it makes it before current time and if so makle it next day
          if (calCurrentTime.after(calNextNotificationTime))
            calNextNotificationTime.add(Calendar.DAY_OF_YEAR, 1);
        }
        else if (primaryRepeatFreq.equalsIgnoreCase("WEEKLY")) {
          //give it the day of week the user set for this reminder
          calNextNotificationTime.set(Calendar.DAY_OF_WEEK, dow);
          //see if it makes it before current time and if so make it next week
          if (calCurrentTime.after(calNextNotificationTime))
            calNextNotificationTime.add(Calendar.WEEK_OF_YEAR, 1);
        }
        else if (primaryRepeatFreq.equalsIgnoreCase("MONTHLY")) {
          //give it the day of week the user set for this reminder
          calNextNotificationTime.set(Calendar.DAY_OF_WEEK, dow);
          //see if it makes it before current time and if so make it next month
          if (calCurrentTime.after(calNextNotificationTime))
            calNextNotificationTime.add(Calendar.MONTH, 1);
        }
        else if (primaryRepeatFreq.equalsIgnoreCase("YEARLY")) {
          //see if it makes it before current time and if so make it next year
          if (calCurrentTime.after(calNextNotificationTime))
            calNextNotificationTime.add(Calendar.YEAR, 1);
        }
        else if (primaryRepeatFreq.equalsIgnoreCase("CUSTOM")) {
          long lNextNotificationTimeInMillis = Long.parseLong(nextNotificationTimeInMillis);
          lNextNotificationTimeInMillis = calcNextCustomTime(lNextNotificationTimeInMillis, auxTimeInfo);
          calNextNotificationTime.setTimeInMillis(lNextNotificationTimeInMillis);
          //see if next notification time is still behind current time
          if (calCurrentTime.after(calNextNotificationTime)) {
            //set it to current time
            calNextNotificationTime = Calendar.getInstance();
            lNextNotificationTimeInMillis = calNextNotificationTime.getTimeInMillis();
            //add the custom interval to it
            lNextNotificationTimeInMillis = calcNextCustomTime(lNextNotificationTimeInMillis, auxTimeInfo);
            calNextNotificationTime.setTimeInMillis(lNextNotificationTimeInMillis);
          }
        }
      }
      else
        calNextNotificationTime.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));
      Log.d("MainActivity", "re-scheduled repeating primary primary notification of \"" + ntfDesc + "\" for " +
        ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()));

      sendDebugMsg(this, "MainActivity: re-scheduled repeating primary notification of \"" + ntfDesc + "\" for " +
        ReminderScheduler.logDateFormat.format(calNextNotificationTime.getTime()) + ". request code = " + reqCode);
    }
    else {
      Log.d("MainActivity", "re-scheduled repeating primary primary notification of \"" + ntfDesc + "\" for " +
        ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()));
      sendDebugMsg(this, "MainActivity: re-scheduled primary notification of \"" + ntfDesc + "\" for " +
        ReminderScheduler.logDateFormat.format(calNextNotificationTime.getTime()) + ". request code = " + reqCode);
    }
    nextNotificationTimeInMillis = "" + calNextNotificationTime.getTimeInMillis();

    //uncomment for debugging
/*
        String sCurrTime = dateFormat.format(calCurrentTime.getTime());
        String sNextNotificationTime = dateFormat.format(calNextNotificationTime.getTime());
*/
    //long when = calNextNotificationTime.getTimeInMillis() - System.currentTimeMillis();

    //alarmManager.setExact(AlarmManager.RTC_WAKEUP, calNextNotificationTime.getTimeInMillis(), broadcast);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calNextNotificationTime.getTimeInMillis(), broadcast);
    else
      alarmManager.setExact(AlarmManager.RTC_WAKEUP, calNextNotificationTime.getTimeInMillis(), broadcast);

    //add new entry into database
    updateDB(STATE, "ON", "" + reqCode);
    updateDB(NTFDATETIME, "" + nextNotificationTimeInMillis, "" + reqCode);

  }

  static int getThemeColor(int resourceName, Context ctx) {
    TypedValue typedValue = new TypedValue();
    Resources.Theme theme = ctx.getTheme();
    theme.resolveAttribute(resourceName, typedValue, true);
    TypedArray arr = ctx.obtainStyledAttributes(typedValue.data, new int[]{resourceName});
    return arr.getColor(0, -1);
  }


  String getReminderTimeString(Calendar nextNotifyTime, boolean bExpired) {
    //display debug info
    Calendar currentTime = Calendar.getInstance();
//        TextView debugView = (TextView) findViewById(R.id.dbgView);
    DateFormat dateDebugFormat;
    String[] auxDataParts = auxTimeInfo.split(":");

    if (primaryRepeatFreq.equalsIgnoreCase("custom") &&
      (auxDataParts[0].equalsIgnoreCase("seconds") || auxDataParts[0].equalsIgnoreCase("minutes")))
      dateDebugFormat = new SimpleDateFormat("h:mm:ss aaa, E MMM d yyyy");
    else
      dateDebugFormat = new SimpleDateFormat("h:mm aaa, E MMM d yyyy");

    double diff = (double) (nextNotifyTime.getTimeInMillis() - currentTime.getTimeInMillis()) / 1000;
    double numDays = (double) diff / 86400;
    double numHours = (double) diff / 3600;
    double numMin = (double) diff / 60;
    DecimalFormat numformat = new DecimalFormat("#.##");
    String debugString = "";
    String reminderFooter;
    if (bExpired)
      reminderFooter = "Last reminder ";
    else
      reminderFooter = "Next reminder ";

    //get the days from calendar objects, for calculating whether reminder is
    // today, tomorrow, or more days from now. this is for determining proper
    // user-friendly text to display
    int currYear = currentTime.get(Calendar.YEAR);
    int reminderYear = nextNotifyTime.get(Calendar.YEAR);

    if (bExpired) {
      debugString = reminderFooter;
    }
    else {
      //check if years are the same
      if (currYear == reminderYear) {
        int currDayOfYear = currentTime.get(Calendar.DAY_OF_YEAR);
        int reminderDayOfYear = nextNotifyTime.get(Calendar.DAY_OF_YEAR);
        //check if days are the same
        if (currDayOfYear == reminderDayOfYear) {
          // this must be today
          reminderFooter += "today ";
        }
        else if ((reminderDayOfYear - currDayOfYear) == 1) {
          // this must be tomorrow
          reminderFooter += "tomorrow ";
        }
        else
          reminderFooter += " in ";
      }
      else
        reminderFooter += " in ";

      if (!(reminderFooter.contains("today") || reminderFooter.contains("tomorrow"))) {
        if (numDays >= 2) {
          debugString = reminderFooter + numformat.format(numDays) + " days";
        }
        else {
          if (numHours >= 2)
            debugString = reminderFooter + numformat.format(numHours) + " hours";
          else
            debugString = reminderFooter + numformat.format(numMin) + " minutes";
        }
      }
      else
        debugString = reminderFooter;
    }

    debugString += "\nat " + dateDebugFormat.format(nextNotifyTime.getTime());
    return debugString;
  }

  void updateReminderRowText(boolean bExpired_, TextView timeTV, Context ctx) {
    DateFormat dtf = new SimpleDateFormat("h:mm aaa, E MMM dd ''yy");
    Calendar nextNotifyTime = Calendar.getInstance();
    nextNotifyTime.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));

    String reminderText;
    String rightText = "expired";
    float rs = 1.20f;

    String primaryFreqText;
    if (primaryRepeatFreq.equalsIgnoreCase("custom")) {
      String[] auxTimeInfoParts = auxTimeInfo.split(":");
      int amount = Integer.parseInt(auxTimeInfoParts[1]);
      if (amount == 1) {
        primaryFreqText = "\n occurs every " + auxTimeInfoParts[0].substring(0, auxTimeInfoParts[0].length() - 1);
      }
      else {
        primaryFreqText = "\n occurs every " + amount + " " + auxTimeInfoParts[0];
      }
    }
    else if (primaryRepeatFreq.equalsIgnoreCase("notify once")) {
      primaryFreqText = "\none time notification";
    }
    else {
      primaryFreqText = "\n occurs " + primaryRepeatFreq;
    }

    if (notificationType.equalsIgnoreCase("both") || notificationType.equalsIgnoreCase("sound alarm"))
      primaryFreqText += "   audible";
    else
      primaryFreqText += "   silent";
    String nextReminderText;

    nextReminderText = getReminderTimeString(nextNotifyTime, bExpired_);

    //String nextReminderText = "Next reminder: " + dtf.format(nextNotifyTime.getTime());
    String secondaryReminderText = "\nkeep reminding every " + secondaryRepeatFreq + " when notification unackowledged\n";

    if (bExpired_)
      reminderText = ntfDesc + "  " + rightText + "\n\n" + nextReminderText + secondaryReminderText +
        primaryFreqText;
    else
      reminderText = ntfDesc + "\n\n" + nextReminderText + secondaryReminderText + primaryFreqText;


    SpannableString formattedText = new SpannableString(reminderText);
    int keepLineIdx = reminderText.indexOf("keep");
    String keepString = reminderText.substring(keepLineIdx);
    int nlidx = keepString.indexOf('\n');
    formattedText.setSpan(new RelativeSizeSpan(.85f), keepLineIdx, keepLineIdx + nlidx, 0);
    formattedText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorSubtext)), keepLineIdx, keepLineIdx + nlidx, 0);
    //Typeface myTypeface = create(ResourcesCompat.getFont(context, R.font.acme), ITALIC);
    formattedText.setSpan(new StyleSpan(Typeface.ITALIC), keepLineIdx, keepLineIdx + nlidx, 0);
    formattedText.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ntfDesc.length(), 0);
    formattedText.setSpan(new RelativeSizeSpan(rs), 0, ntfDesc.length(), 0);
    formattedText.setSpan(new StyleSpan(BOLD), 0, ntfDesc.length(), 0);
    if (bExpired_) {
      formattedText.setSpan(new ForegroundColorSpan(Color.RED),
        ntfDesc.length() + 2, ntfDesc.length() + rightText.length() + 2,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      //formattedText.setSpan(new RelativeSizeSpan(rs), ntfDesc.length() + rightText.length() + 2, reminderText.length(), 0);
    }
/*
        else
            formattedText.setSpan(new RelativeSizeSpan(rs), ntfDesc.length() + 1, reminderText.length(), 0);
*/
    int repeatsIdx = reminderText.indexOf(" occurs");
    if (repeatsIdx > -1) {
      Drawable myIcon = getResources().getDrawable(R.drawable.ic_repeat);
      myIcon.setBounds(0, 0, timeTV.getLineHeight(), timeTV.getLineHeight());
      myIcon.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.colorDarkGray), PorterDuff.Mode.SRC_ATOP));
      formattedText.setSpan(new ImageSpan(myIcon, ImageSpan.ALIGN_BOTTOM),
        repeatsIdx, repeatsIdx + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    int soundIdx = reminderText.indexOf(" audible");
    if (soundIdx > -1) {
      Drawable myIcon = getResources().getDrawable(R.drawable.ic_volume);
      myIcon.setBounds(0, 0, timeTV.getLineHeight(), timeTV.getLineHeight());
      myIcon.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.colorDarkGray), PorterDuff.Mode.SRC_ATOP));
      formattedText.setSpan(new ImageSpan(myIcon, ImageSpan.ALIGN_BOTTOM),
        soundIdx, soundIdx + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    int silentIdx = reminderText.indexOf(" silent");
    if (silentIdx > -1) {
      Drawable myIcon = getResources().getDrawable(R.drawable.ic_silent);
      myIcon.setBounds(0, 0, timeTV.getLineHeight(), timeTV.getLineHeight());
      myIcon.setColorFilter(new PorterDuffColorFilter(getResources().getColor(R.color.colorDarkGray), PorterDuff.Mode.SRC_ATOP));
      formattedText.setSpan(new ImageSpan(myIcon, ImageSpan.ALIGN_BOTTOM),
        silentIdx, silentIdx + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    timeTV.setText(formattedText);
  }

  void createNewRow(final TableLayout tl, final TableLayout.LayoutParams lparams) {

    boolean bExpired_ = false;

    TableRow row = new TableRow(this);
    row.setClickable(true);  //allows you to select a specific row
    Drawable shadowDrawable = getResources().getDrawable(android.R.drawable.dialog_holo_light_frame);
    row.setBackground(shadowDrawable);
    lparams.setMargins(10, 10, 10, 10);
    //row.setPadding(0,0,0,20);
    row.setLayoutParams(lparams);

    SwitchCompat onOffToggleSwitch = (SwitchCompat) getLayoutInflater().inflate(R.layout.switch_template, null);
    TableRow.LayoutParams switchParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    switchParams.weight = 1;
    //switchParams.topMargin = 20;
    onOffToggleSwitch.setBackgroundColor(Color.WHITE);

    onOffToggleSwitch.setLayoutParams(switchParams);
    //onOffToggleSwitch.setBackgroundColor(getResources().getColor(R.color.colorRegReminder));
    Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);//the same as up
    boolean isSet = (PendingIntent.getBroadcast(MainActivity.this, reqCode, intent, PendingIntent.FLAG_NO_CREATE) != null);

    if (primaryRepeatFreq.equalsIgnoreCase("notify once") && System.currentTimeMillis() > Long.parseLong(nextNotificationTimeInMillis)) {
      bExpired_ = true;
    }

    Calendar calNextNotificationTime = Calendar.getInstance();
    calNextNotificationTime.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));

    Log.d("MainActivity", "Displaying reminder \"" + ntfDesc + "\", req code " + reqCode +
      ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()));


    if (bExpired_) {
      onOffToggleSwitch.setChecked(false);
      //see if alarm associated with reqID is set
      if (isSet) {
        Log.d("MainActivity", "Reminder \"" + ntfDesc + "\", req code " + reqCode +
          ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
          " is expired and will be canceled.");
        cancelReminder(reqCode);
      }
      if (strState.equalsIgnoreCase("ON")) {
        Log.d("MainActivity", "Reminder \"" + ntfDesc + "\", req code " + reqCode +
          ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
          " is expired.");
        updateDB(STATE, "OFF", "" + reqCode);

      }
      else {
        Log.d("MainActivity", "Reminder \"" + ntfDesc + "\", req code " + reqCode +
          ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
          " is expired and not currently set.");
      }
    }
    else {
      Log.d("MainActivity", "Reminder \"" + ntfDesc + "\", req code " + reqCode +
        ". Last scheduled for " + ReminderScheduler.dateFormat.format(calNextNotificationTime.getTime()) +
        " is not expired.");
      if (strState.equalsIgnoreCase("ON")) {
        onOffToggleSwitch.setChecked(true);
        if (!isSet) {
          Log.d("MainActivity", "Reminder \"" + ntfDesc + "\", req code " + reqCode + " is not scheduled. Needs to be rescheduled.");
          scheduleNotification(reqCode);
        }
      }
      else {
        onOffToggleSwitch.setChecked(false);
      }
    }


    TextView timeTV = new TextView(this);
    updateReminderRowText(bExpired_, timeTV, this);
    TableRow.LayoutParams timeTVparams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    timeTVparams.weight = 6;
    //timeTVparams.setMargins(10,2,5,0);
    timeTV.setLayoutParams(timeTVparams);
    timeTV.setPadding(10, 2, 10, 2);
    timeTV.setBackgroundColor(Color.WHITE);

    //timeTV.setBackground(shadowDrawable);

    onOffToggleSwitch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        //get the index of the row in the TableLayout
        ViewGroup rowView = (ViewGroup) v.getParent();
        int idx = tl.indexOfChild(rowView);

        //use the row index to reference the associated onbject in the LinkedList.
        //we need to get the linkedlist element b/c it contains the request ID
        //for the PendingIntent
        Ntf reminder = ntfList.get(idx);

        //notifyBtn object will be toggled on or off and different color will be applied accordingly
        SwitchCompat onOffSwitch = (SwitchCompat) v;

        //read in the latest database data for this row
        setAllRowDBfields("" + reminder.reqID);

        //see if alarm associated with reqID is set
        Intent intent = new Intent(MainActivity.this, AlarmReceiver.class);//the same as up
        boolean isSet =
          (PendingIntent.getBroadcast(MainActivity.this, reminder.reqID, intent,
                PendingIntent.FLAG_NO_CREATE) != null);//just changed the flag
        if (isSet) {
          //v.setBackgroundColor(getResources().getColor(R.color.colorRegReminder));
          //notifyBtn.setText("OFF");
          onOffSwitch.setChecked(false);
          cancelReminder(reminder.reqID);
          if (strState.equalsIgnoreCase("ON")) {
            updateDB(STATE, "OFF", "" + reminder.reqID);
          }
        }
        else {
          Cursor res = myDb.getAllRowData("" + reminder.reqID);
          myDb.close();

          calNotificationDate = Calendar.getInstance();
          calNotificationDate.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));
          boolean bExpired = false;

          if (primaryRepeatFreq.equalsIgnoreCase("notify once") && System.currentTimeMillis() > Long.parseLong(nextNotificationTimeInMillis)) {
            bExpired = true;
            onOffSwitch.setChecked(false);
          }
          else {
            //v.setBackgroundColor(getResources().getColor(R.color.colorOn));
            //notifyBtn.setText("ON");
            onOffSwitch.setChecked(true);

            //schedule next notification
            scheduleNotification(reminder.reqID);
            //get the reminder textview to update it with the new time
            TextView textView = (TextView) rowView.getChildAt(0);
            updateReminderRowText(false, textView, getApplicationContext());
          }
        }
      }
    });

    row.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        iSelectedRowIdx = tl.indexOfChild(v);
        Ntf reminder = ntfList.get(iSelectedRowIdx);
        Intent editReminder = new Intent(MainActivity.this, ReminderScheduler.class);
        editReminder.putExtra(NOTIFICATION_ID, "" + reminder.reqID);
        editReminder.putExtra(NOTIFICATION_NUM, "Primary Notification");
        //startActivity(scheduleReminder);
        startActivityForResult(editReminder, 2);
        Log.d("event", "click");
      }
    });

    row.setOnLongClickListener(new View.OnLongClickListener() {

      @Override
      public boolean onLongClick(View v) {
        Log.d("event", "long click");
        //activate the context menu in the action bar.
        if (mActionMode != null)
          return false;
        mActionMode = startSupportActionMode(mActionModeCallback);

        //get the index of the row in the TableLayout
        iSelectedRowIdx = tl.indexOfChild(v);

        TableRow tablerow = (TableRow) v;
        TextView view = (TextView) tablerow.getChildAt(0);
        view.setBackgroundColor(getResources().getColor(R.color.colorTouch));
        return true;
      }
    });


    row.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
          case MotionEvent.ACTION_DOWN: {
            TableRow tablerow = (TableRow) v;
            TextView view = (TextView) tablerow.getChildAt(0);
            if (mActionMode == null) {
              view.setBackgroundColor(getResources().getColor(R.color.colorVeryLightGrey));
            }
            else {
              mActionMode.finish();
            }

            Log.d("event", "down");
            break;
          }
          case MotionEvent.ACTION_UP: {
            if (mActionMode == null) {
              try {
                Thread.sleep(delay);
              }
              catch (Exception e) {
              }
              TableRow tablerow = (TableRow) v;
              TextView view = (TextView) tablerow.getChildAt(0);
              view.setBackgroundColor(getResources().getColor(R.color.colorRegReminder));

            }
            Log.d("event", "up");
            break;
          }
          //need this event because it is being received when user scrolls.
          //the parent ScollView obj the ScrollView intercepts/handles the scroll events
          //and passes the child objects the ACTION_CANCEL
          case MotionEvent.ACTION_CANCEL: {
            Log.d("event", "cancel");
            TableRow tablerow = (TableRow) v;
            TextView view = (TextView) tablerow.getChildAt(0);
            view.setBackgroundColor(getResources().getColor(R.color.colorRegReminder));
            break;
          }
        }
        return false;
      }
    });


    row.setPadding(5, 5, 5, 5);
    row.addView(timeTV);
    row.addView(onOffToggleSwitch);
    tl.addView(row, 0);


  }

  void setAllRowDBfields(String strReqCode) {
    myDb = RemindersStorage.getInstance(this);
    Cursor res = myDb.getAllRowData(strReqCode);
    if (res.getCount() == 0) {
      // show message
      //showMessage("Alert","No reminders to show");
    }
    else {
      while (res.moveToNext()) {
        //get the data
        ntfDesc = res.getString(RemindersStorage.FIELDS.NAME);
        nextNotificationTimeInMillis = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
        reqCode = res.getInt(RemindersStorage.FIELDS.REQCODE);
        primaryRepeatFreq = res.getString(RemindersStorage.FIELDS.PRIMARYREMINDERFREQ);
        secondaryRepeatFreq = res.getString(RemindersStorage.FIELDS.SECONDARYREMINDERFREQ);
        strState = res.getString(RemindersStorage.FIELDS.STATE);
        auxTimeInfo = res.getString(RemindersStorage.FIELDS.AUXTIMEINFO);
        notificationType = res.getString(RemindersStorage.FIELDS.NOTIFICATIONTYPE);
      }
      // Show all data
      // showMessage("Data", buffer.toString());
    }
    myDb.close();
  }

  void showReminders() {
    myDb = RemindersStorage.getInstance(this);
    Cursor res = myDb.getAllData();
    if (res.getCount() == 0) {
      // show message
      //showMessage("Alert","No reminders to show");
    }
    else {

      TableLayout tl = null;
      TableLayout.LayoutParams lparams = null;
      tl = (TableLayout) findViewById(R.id.tableLayout);
      lparams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

      //StringBuffer buffer = new StringBuffer();
      while (res.moveToNext()) {
        //get the data
        ntfDesc = res.getString(RemindersStorage.FIELDS.NAME);
        nextNotificationTimeInMillis = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
        reqCode = res.getInt(RemindersStorage.FIELDS.REQCODE);
        primaryRepeatFreq = res.getString(RemindersStorage.FIELDS.PRIMARYREMINDERFREQ);
        secondaryRepeatFreq = res.getString(RemindersStorage.FIELDS.SECONDARYREMINDERFREQ);
        strState = res.getString(RemindersStorage.FIELDS.STATE);
        auxTimeInfo = res.getString(RemindersStorage.FIELDS.AUXTIMEINFO);
        notificationType = res.getString(RemindersStorage.FIELDS.NOTIFICATIONTYPE);
        createNewRow(tl, lparams);
        Ntf reminder = new Ntf();
        reminder.reqID = reqCode;
        reminder.name = ntfDesc;
        ntfList.add(0, reminder);
      }
      // Show all data
      // showMessage("Data", buffer.toString());
    }
    myDb.close();
  }


  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    // Check which request we're responding to
    if (requestCode == 2) {

      // Make sure the request was successful
      if (resultCode == RESULT_OK) {
        myDb = RemindersStorage.getInstance(this);
        reqCode = Integer.parseInt(data.getStringExtra("request ID"));
        Cursor res = myDb.getAllRowData("" + reqCode);
        if (res.getCount() == 0) {
          // show message
          //showMessage("Alert","No reminders to show");
        }
        else {
          TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
          TableLayout.LayoutParams lparams = new TableLayout.LayoutParams(
            TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);
          //StringBuffer buffer = new StringBuffer();
          while (res.moveToNext()) {
            //get the data
            ntfDesc = res.getString(RemindersStorage.FIELDS.NAME);
            nextNotificationTimeInMillis = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
            primaryRepeatFreq = res.getString(RemindersStorage.FIELDS.PRIMARYREMINDERFREQ);
            secondaryRepeatFreq = res.getString(RemindersStorage.FIELDS.SECONDARYREMINDERFREQ);
            auxTimeInfo = res.getString(RemindersStorage.FIELDS.AUXTIMEINFO);
            notificationType = res.getString(RemindersStorage.FIELDS.NOTIFICATIONTYPE);
          }
          // Show all data
          // showMessage("Data", buffer.toString());
        }
        myDb.close();

        long lNotificationTime = Long.parseLong(nextNotificationTimeInMillis);
        calNotificationDate = Calendar.getInstance();
        calNotificationDate.setTimeInMillis(lNotificationTime);
        strState = "ON";

        TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
        TableLayout.LayoutParams lparams = new TableLayout.LayoutParams(
          TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

        createNewRow(tl, lparams);
        calNotificationDate.setTimeInMillis(lNotificationTime);
      }
    }
  }

  private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      mode.getMenuInflater().inflate(R.menu.context_action_menu, menu);
      return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      int id = item.getItemId();
      //use the row index to reference the associated object in the LinkedList.
      //we need to get the linkedlist element b/c it contains the request ID
      //for the PendingIntent
      Ntf reminder = ntfList.get(iSelectedRowIdx);

      //noinspection SimplifiableIfStatement
      if (id == R.id.edit) {
        Intent editReminder = new Intent(MainActivity.this, ReminderScheduler.class);
        editReminder.putExtra(NOTIFICATION_ID, "" + reminder.reqID);
        editReminder.putExtra(NOTIFICATION_NUM, "Primary Notification");

        //startActivity(scheduleReminder);
        startActivityForResult(editReminder, 2);

        mode.finish();
        return true;
      }
      else if (id == R.id.delete) {
        Intent cancelIntent = new Intent(MainActivity.this, AlarmReceiver.class);//the same as up
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, reminder.reqID,
          cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);//the same as up
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);//important
        pendingIntent.cancel();//important

        sendDebugMsg(MainActivity.this, "MainActivity: canceling notification, request code = " + reminder.reqID);

        //delete the reminder from the database and from linked list
        myDb = RemindersStorage.getInstance(MainActivity.this);
        myDb.deleteData("" + reminder.reqID);
        myDb.close();
        //now remove same item from the linked list
        ntfList.remove(iSelectedRowIdx);

        //remove the associated row item from the GUI
        TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
        tl.removeViewAt(iSelectedRowIdx);

        iSelectedRowIdx = -1;
        mode.finish();

        return true;
      }
      else {
        return false;
      }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
      Log.d("ActionMode", "destroy");
      mActionMode = null;
      if (iSelectedRowIdx != -1) {
        TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
        TableRow reminderEntry = (TableRow) tl.getChildAt(iSelectedRowIdx);
        TextView rmdTV = (TextView) reminderEntry.getChildAt(0);
        rmdTV.setBackgroundColor(getResources().getColor(R.color.colorRegReminder));
      }
    }

  };


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      Intent settingsAct = new Intent(MainActivity.this, SettingsActivity.class);
      startActivity(settingsAct);
      //return true;
    }
    else if (id == R.id.history) {
      FragmentManager mgr = getFragmentManager();
      History histDlg = new History();
      histDlg.show(mgr, "");
    }
    else if (id == R.id.purge_all) {
      myDb = RemindersStorage.getInstance(this);
      Cursor res = myDb.getAllData();
      if (res.getCount() == 0) {
        // show message
        //showMessage("Alert","No reminders to show");
      }
      else {
        //StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
          //get the data
          ntfDesc = res.getString(RemindersStorage.FIELDS.NAME);
          nextNotificationTimeInMillis = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
          reqCode = res.getInt(RemindersStorage.FIELDS.REQCODE);
          Intent notifyIntent = new Intent(MainActivity.this, AlarmReceiver.class);
          PendingIntent broadcast = PendingIntent.getBroadcast(MainActivity.this, reqCode, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
          AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
          alarmManager.cancel(broadcast);
          sendDebugMsg(this, "MainActivity: canceling notification. req code = " + reqCode);
          broadcast.cancel();
        }
        // Show all data
        // showMessage("Data", buffer.toString());
        TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
        tl.removeAllViews();
        ntfList.clear();

        deleteDatabase(RemindersStorage.DATABASE_NAME);
        myDb.close();
      }

      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  public void onResume() {
    super.onResume();

    if (bPause) {
      TableLayout tl = (TableLayout) findViewById(R.id.tableLayout);
      tl.removeAllViews();
      showMessage("Alert", "Back in main activity", getApplication());
      showReminders();
      bPause = false;
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    showMessage("Alert", "destroying MainActivity", getApplication());
    //Toast.makeText(ReminderScheduler.this, "ending reminder schedule screen", Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onPause() {
    super.onPause();
    showMessage("Alert","pausing MainActivity", getApplication());
    bPause = true;
  }

  @Override
  protected void onStop(){
    super.onStop();
    showMessage("Alert", "stopping MainActivity", getApplication());
  }

  static public void showMessage(String title, String Message, Context ctx) {
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
    builder.setCancelable(true);
    builder.setTitle(title);
    builder.setMessage(Message);
    //builder.show();
    System.out.println(Message + " !!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
}
