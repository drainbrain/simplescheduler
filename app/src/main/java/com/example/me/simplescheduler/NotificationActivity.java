package com.example.me.simplescheduler;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.me.simplescheduler.AlarmReceiver.debugMesg;
import static com.example.me.simplescheduler.AlarmReceiver.sendDebugMsg;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_ID;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NAME;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NUM;
import static com.example.me.simplescheduler.ReminderScheduler.iGlobalRequestCode;
import static com.example.me.simplescheduler.RemindersStorage.STATE;

public class NotificationActivity extends AppCompatActivity {

    boolean bKeepNotifying = true;
    Calendar oldNotfTime;
    long secondsFromNow=0;
    final static String filename = "notificationHist.txt";
    static DateFormat dateLogFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aaa z");;
    private String notificationName;
    private String reqID;
    private String notificationNum;
    private String notificationType;
    private Ringtone ring = null;
    private int iNumSecondsTillNextNotification;

    RemindersStorage myDb;
    private Calendar newNotfTime;
    private int iSecondaryReminderReqCode;

    public void cancelNotification(){
        Intent notifyIntent = new Intent(this, AlarmReceiver.class);

        PendingIntent broadcast = PendingIntent.getBroadcast(this, Integer.parseInt(reqID), notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        sendDebugMsg(this, "NotificationActivity: canceling notification " + "\"" + notificationName +
                "\". request code = " + reqID);

        alarmManager.cancel(broadcast);
        broadcast.cancel();
        bKeepNotifying = false;
        if(ring != null)
            ring.stop();

        //update database
        myDb = RemindersStorage.getInstance(this);
        myDb.updateDBfield(STATE, "OFF", reqID);
        finish();
    }

    AudioManager am;
    int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        bKeepNotifying = false;
        // schedule next notification
        ////////////////////////////////////////////////////

        //1st get the intent data to determine where notification came from
        Bundle dataBundle = getIntent().getExtras();

        if(dataBundle!=null) {
            reqID = (String) dataBundle.get(NOTIFICATION_ID);
            notificationNum = (String) dataBundle.get(NOTIFICATION_NUM);
            notificationName = (String) dataBundle.get(NOTIFICATION_NAME);

            //if(notificationNum.equals("Primary Notification")) {

            newNotfTime = Calendar.getInstance();
            oldNotfTime = Calendar.getInstance();

            RemindersStorage myDb = RemindersStorage.getInstance(this);
            Cursor res = myDb.getAllRowData(reqID);
            if (res != null) {
                res.moveToFirst();
                String freq = res.getString(RemindersStorage.FIELDS.PRIMARYREMINDERFREQ);
                String secondaryFreq = res.getString(RemindersStorage.FIELDS.SECONDARYREMINDERFREQ);
                setRepeatReminderFreq(secondaryFreq);
                String ntfTime = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
                notificationType = res.getString(RemindersStorage.FIELDS.NOTIFICATIONTYPE);
                oldNotfTime.setTimeInMillis(Long.parseLong(ntfTime));
                newNotfTime.setTimeInMillis(Long.parseLong(ntfTime));
                if (!freq.equalsIgnoreCase("notify once")) {
                    if (freq.equalsIgnoreCase("daily")) {
                        newNotfTime.add(Calendar.DAY_OF_YEAR, 1);
                    }
                    else if (freq.equalsIgnoreCase("weekly")) {
                        newNotfTime.add(Calendar.WEEK_OF_YEAR, 1);
                    }
                    else if (freq.equalsIgnoreCase("monthly")) {
                        newNotfTime.add(Calendar.MONTH, 1);
                    }
                    else if (freq.equalsIgnoreCase("yearly")) {
                        newNotfTime.add(Calendar.YEAR, 1);
                    }
                    else if (freq.equalsIgnoreCase("custom")) {
                        String auxTimeData = res.getString(RemindersStorage.FIELDS.AUXTIMEINFO);
                        long lNextNotifyTime = MainActivity.calcNextCustomTime(oldNotfTime.getTimeInMillis(), auxTimeData);
                        newNotfTime.setTimeInMillis(lNextNotifyTime);
                    }
                    myDb.close();
                    if(notificationNum.equals("Primary Notification"))
                        scheduleNextNotification();
                }
                else {
                    myDb.close();
                }

                //determine notification type then take appropriate corresponding action
                if (notificationType.equalsIgnoreCase("sound alarm") || notificationType.equalsIgnoreCase("both")) {
                    try {
                        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        mode = am.getMode();
                        if (mode != AudioManager.RINGER_MODE_NORMAL)
                            am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                        ring = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        ring.play();

                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                ring.stop();
                                am.setRingerMode(mode);
                            }
                        };
                        Timer timer = new Timer();
                        timer.schedule(task, 10000);

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (notificationType.equalsIgnoreCase("visual alarm") || notificationType.equalsIgnoreCase("both")) {
                    final Window win = getWindow();
                    win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                    //win.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
                    //win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                    win.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                    win.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            }
            else {
                myDb.close();
            }

            Toast.makeText(NotificationActivity.this, "scheduled new notification", Toast.LENGTH_SHORT).show();
            //} if(notificationNum.equals("Primary Notification")) {

            //debugMesg("started");

            DateFormat dtf = new SimpleDateFormat("hh:mm aaa z, d MMM ''yy");
            Calendar currentTime = Calendar.getInstance();
            TextView timeStampTV = (TextView) findViewById(R.id.timeStampView);
            timeStampTV.setText(dtf.format(currentTime.getTimeInMillis()));

            TextView descTV = (TextView) findViewById(R.id.nameText);
            descTV.setText(notificationName);
            descTV.setFocusable(false);

            final TypedArray ta = getTheme().obtainStyledAttributes(new int[] {android.R.attr.actionBarSize});
            float temp = ta.getDimension(0, 0)/3;
            int actionBarHeight = (int) temp;
            Drawable myIcon = getResources().getDrawable(R.drawable.ic_notifications_active);
            myIcon.setBounds(0,0,actionBarHeight,actionBarHeight);

            myIcon.setColorFilter( new PorterDuffColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP));

            SpannableString titleTextSpan = new SpannableString("  " + notificationNum);
            titleTextSpan.setSpan(new ImageSpan(myIcon, ImageSpan.ALIGN_BASELINE), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );

            setTitle(titleTextSpan);

            //getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);

            WindowManager.LayoutParams p = getWindow().getAttributes();
/*
            p.width = ViewGroup.LayoutParams.MATCH_PARENT;
            p.height = ViewGroup.LayoutParams.WRAP_CONTENT;
*/
            p.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN;
            getWindow().setAttributes(p);

            //re-schedule notification, this time secondary
            Intent notifyIntent = new Intent(NotificationActivity.this, AlarmReceiver.class);
            notifyIntent.putExtra(NOTIFICATION_NUM, "Repeat Notification");
            notifyIntent.putExtra(NOTIFICATION_NAME, notificationName);
            iSecondaryReminderReqCode = iGlobalRequestCode * 7;
            notifyIntent.putExtra(NOTIFICATION_ID, reqID);

            PendingIntent broadcast = PendingIntent.getBroadcast(NotificationActivity.this, iSecondaryReminderReqCode,
                    notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            //long repeatNotificationTime = System.currentTimeMillis() + secondsFromNow * 1000;
            long repeatNotificationTime = System.currentTimeMillis() + (secondsFromNow * 1000);
            //alarmManager.setExact(AlarmManager.RTC_WAKEUP, repeatNotificationTime, broadcast);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, repeatNotificationTime, broadcast);
            else
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, repeatNotificationTime, broadcast);


            Toast.makeText(NotificationActivity.this, "scheduling refresh of old notification", Toast.LENGTH_SHORT).show();
            oldNotfTime = Calendar.getInstance();
            oldNotfTime.setTimeInMillis(repeatNotificationTime);
            sendDebugMsg(NotificationActivity.this, "NotificationActivity: " + "\"" + notificationName +
                    "\" secondary notification scheduled for " + ReminderScheduler.logDateFormat.format(oldNotfTime.getTime()) +
                    ". request code = " + iSecondaryReminderReqCode);
        }

        findViewById(R.id.cancelNotificationBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelNotification();
            }
        });



        Button ackBtn = (Button) findViewById(R.id.ackBtn);
        ackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bKeepNotifying = false;
                debugMesg("acknowledged \'" + notificationName + "\'");
                cancelPendingIntent();
                finish();
            }

        });

        ackBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Button view = (Button) v;
                        view.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        Button view = (Button) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return false;
            }
        });

    }

    void cancelPendingIntent(){
        Intent cancelIntent = new Intent(NotificationActivity.this, AlarmReceiver.class);//the same as up
        PendingIntent pendingIntent = PendingIntent.getBroadcast(NotificationActivity.this,
                iSecondaryReminderReqCode, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT);//the same as up
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);//important
        pendingIntent.cancel();//important
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("NotificationActivity","resume!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("NotificationActivity","stop--------------------------------");
    }

    void scheduleNextNotification(){
        RemindersStorage myDb = RemindersStorage.getInstance(this);
        SQLiteDatabase db = myDb.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("ntf_date_time", newNotfTime.getTimeInMillis()); //These Fields should be your String values of actual column names
        db.update(RemindersStorage.TABLE_NAME, cv, " request_code = ?", new String[] {reqID});
        myDb.close();

        //////////////////////////////////////////////////////////////////////
        //reset alarm if it is repeated.

        //data to pass to alert handler
        Intent notifyIntent = new Intent(this, AlarmReceiver.class);
        notifyIntent.putExtra(NOTIFICATION_ID, reqID);
        notifyIntent.putExtra(NOTIFICATION_NUM, "Primary Notification");
        notifyIntent.putExtra(NOTIFICATION_NAME, notificationName);
        //start scheduler
        PendingIntent broadcast = PendingIntent.getBroadcast(this, Integer.parseInt(reqID), notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //alarmManager.setExact(AlarmManager.RTC_WAKEUP, newNotfTime.getTimeInMillis(), broadcast);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, newNotfTime.getTimeInMillis(), broadcast);
        else
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, newNotfTime.getTimeInMillis(), broadcast);

        /////////////////////////
        sendDebugMsg(this, "NotificationActivity: re-scheduled next " + "\"" + notificationName +
                "\" primary notification for " + ReminderScheduler.logDateFormat.format(newNotfTime.getTime()) + ". request code = " + reqID);
    }

    void setRepeatReminderFreq(String freq){
        Log.d("SimpleScheduler","setRepeatReminderFreq() freq=" + freq);
        int numMin = 0;

        if(freq.contains("Hour") || freq.contains("hour")){
            numMin = 60;
        }
        else if(freq.contains("Day") || freq.contains("day")){
            numMin = 60 * 24;
        }
        else{
            int endidx = freq.indexOf(' ');
            numMin = Integer.parseInt(freq.substring(0,endidx));
        }
        secondsFromNow = numMin * 60;
    }

    @Override
    protected void onDestroy() {
        if(ring != null)
            ring.stop();

        //start scheduler
        debugMesg("\'" + notificationName + "\' done!");
        Log.d("onDestroy()","bKeepNotifying=" + bKeepNotifying);
        //set timer for next notification
        if( bKeepNotifying ) {
            bKeepNotifying = false;
            Intent notifyIntent = new Intent(this, AlarmReceiver.class);
            notifyIntent.putExtra(NOTIFICATION_NUM, "Repeat Notification");
            notifyIntent.putExtra(NOTIFICATION_NAME, notificationName);
            int iNewReqCode = iGlobalRequestCode * 7;
            Log.d("onDestroy()","scheduling next secondary notification for " + ((int) secondsFromNow/60) +
                    " min ahead. iSecondaryReminderReqCode=" + iNewReqCode);
            notifyIntent.putExtra(NOTIFICATION_ID, reqID);

            PendingIntent broadcast = PendingIntent.getBroadcast(this, iNewReqCode, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            //long repeatNotificationTime = System.currentTimeMillis() + secondsFromNow * 1000;
            long repeatNotificationTime = System.currentTimeMillis() + (secondsFromNow * 1000);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, repeatNotificationTime, broadcast);
            else
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, repeatNotificationTime, broadcast);

            Toast.makeText(NotificationActivity.this, "scheduling refresh of old notification", Toast.LENGTH_SHORT).show();
            oldNotfTime = Calendar.getInstance();
            oldNotfTime.setTimeInMillis(repeatNotificationTime);
            sendDebugMsg(this, "NotificationActivity: " + "\"" + notificationName +
                    "\" secondary notification starting activity." +
                    ReminderScheduler.logDateFormat.format(oldNotfTime.getTime()) + ". request code = " + iNewReqCode);
        }


        super.onDestroy();
    }




}
