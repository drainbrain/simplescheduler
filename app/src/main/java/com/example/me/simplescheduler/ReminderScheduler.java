package com.example.me.simplescheduler;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.example.me.simplescheduler.AlarmReceiver.sendDebugMsg;


public class ReminderScheduler extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
  Calendar calNotificationDate;
  CheckBox repeatOrNot;
  Spinner dateTimeSpin;
  Spinner secondaryReminderFreqSpin;
  Spinner primaryReminderFreqSpin;
  Spinner notificationTypeSpin;
  String notificationType;
  TextView debugView;
  EditText reminderTitleEditText;
  String reminderDesc;
  String secondaryReminderFreq;
  String primaryReminderFreq;
  String reqID;
  String notificationNum;
  String notificationName;
  boolean bUpdate = false; //flag indicating that this reminder already exists and is being updated
  ImageButton checkib;
  boolean bDateSet = false, bCustomSet = false, bDayOfWeekSet = false, bDayOfMonthSet = false, bDayOfYearSet = false;
  int iDayOfWeek;
  boolean bTitleSet = false;


  final static String NOTIFICATION_NAME = "notificationName";
  final static String NOTIFICATION_NUM = "notificationNum";
  final static String NOTIFICATION_ID = "requestCode";

  static int iGlobalRequestCode = 1;
  private String ntfDesc;
  private String nextNotificationTimeInMillis;
  private int reqCode;
  private String strState;
  private TimePicker simpleTimePicker;
  private DatePicker simpleDatePicker;
  private String auxTimeInfo;
  private EditText customNumET;
  private Spinner customUnitSpin;
  private String customUnits;
  private int customAmt;
  private boolean bReadyToSave;
  private TextView timeText;
  private TextView dateText;
  private boolean bInitMode;
  private int iDayOfMonth;
  private int iDayOfYear;


  public void handleDialogClose(DialogInterface dialog) {
  }

  void setCustomSpinnerChoices() {
    // Spinner Drop down elements
    List<String> categories = new ArrayList<String>();
    categories.add("secs");
    categories.add("mins");
    categories.add("hours");
    categories.add("days");
    // Creating adapter for spinner
    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

    // Drop down layout style - list view with radio button
    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    customUnitSpin.setAdapter(dataAdapter);
  }

  void setDateTimeFootnoteText(boolean init) {
    if (init) {
      timeText = (TextView) findViewById(R.id.timetext);
      dateText = (TextView) findViewById(R.id.datetext);
    }
    DateFormat timeFormat;
    timeFormat = new SimpleDateFormat("hh:mm aaa");
    timeText.setText(timeFormat.format(calNotificationDate.getTime()));
    DateFormat dateFormat = null;
    String dateString = "";
    Calendar currTime = Calendar.getInstance();
    if (primaryReminderFreq.equalsIgnoreCase("daily") || primaryReminderFreq.equalsIgnoreCase("notify once")) {
      dateFormat = new SimpleDateFormat("E MMM d");
      dateString = dateFormat.format(calNotificationDate.getTime());
    }
    else if (primaryReminderFreq.equalsIgnoreCase("custom")) {
      if (bCustomSet)
        dateString = "Every " + customAmt + " " + customUnits;
      else
        dateString = "Not set";
    }
    else if (primaryReminderFreq.equalsIgnoreCase("weekly")) {
      dateFormat = new SimpleDateFormat("E");
      dateString = "Every " + dateFormat.format(calNotificationDate.getTime());
    }
    else if (primaryReminderFreq.equalsIgnoreCase("monthly")) {
      dateFormat = new SimpleDateFormat("d");
      int day = calNotificationDate.get(Calendar.DAY_OF_MONTH);
      if (day == 1 || day == 21) {
        dateString = "Every " + dateFormat.format(calNotificationDate.getTime()) + "st of the month";
      }
      else if (day == 2 || day == 22) {
        dateString = "Every " + dateFormat.format(calNotificationDate.getTime()) + "nd of the month";
      }
      else if (day == 3 || day == 23) {
        dateString = "Every " + dateFormat.format(calNotificationDate.getTime()) + "rd of the month";
      }
      else if ((day > 3 && day < 21) || (day > 23 && day < 31)) {
        dateString = "Every " + dateFormat.format(calNotificationDate.getTime()) + "th of the month";
      }
    }
    else if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
      dateFormat = new SimpleDateFormat("MMM d");
      dateString = "Every " + dateFormat.format(calNotificationDate.getTime());
    }


    dateText.setText(dateString);
  }

  ImageView repeatIV;
  EditText dayOfMonthEdit;

  int getAttribute(int resid) {
    TypedValue typedValue = new TypedValue();
    Resources.Theme theme = getTheme();
    theme.resolveAttribute(resid, typedValue, true);
    TypedArray arr = obtainStyledAttributes(typedValue.data, new int[]{resid});
    return arr.getColor(0, -1);
  }

  DatePickerDialog dpd;


  boolean eventHandler(View v, MotionEvent event, String mode) {
    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN: {
        v.setBackgroundColor(getResources().getColor(R.color.colorVeryLightGrey));
        int iThemeResourceID = android.R.style.Theme_Holo_Light_Dialog;
        Log.d("event", "date set button down");
        //MotionEvent.ACTION_UP Won't get called until the MotionEvent.ACTION_DOWN occurred,
        // a logical explanation for this is that it's impossible for an ACTION_UP to occur
        // if an ACTION_DOWN never occurred before it.
        //This logic enables the developer to block further events after ACTION_DOWN.
        // return true if listener has consumed the event, false if you want to pass on
        // the event to other controls in the hierarchy.
        if (mode.equalsIgnoreCase("time")) {
          TimePickerDialog tpd = new TimePickerDialog(ReminderScheduler.this, iThemeResourceID,
            onTimeSetListener, calNotificationDate.get(Calendar.HOUR_OF_DAY),
            calNotificationDate.get(Calendar.MINUTE), false);
          tpd.show();
        }
        else if (mode.equalsIgnoreCase("date")) {
          if (primaryReminderFreq.equalsIgnoreCase("notify once") || primaryReminderFreq.equalsIgnoreCase("daily")) {
            new DatePickerDialog(ReminderScheduler.this, iThemeResourceID,
              onDateSetListener, calNotificationDate.get(Calendar.YEAR),
              calNotificationDate.get(Calendar.MONTH),
              calNotificationDate.get(Calendar.DAY_OF_MONTH)).show();
          }
          else if (primaryReminderFreq.equalsIgnoreCase("weekly")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View dayOfWeekPickerView = getLayoutInflater().inflate(R.layout.dayofweek_chooser, null);
            //builder.setTitle("Pick the day of week");
            TextView title = new TextView(this);

            // You Can Customise your Title here
            title.setText("Pick day of week");
            title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            title.setPadding(20, 20, 20, 20);
            title.setGravity(Gravity.CENTER);
            title.setTextColor(Color.WHITE);
            title.setTextSize(20);
            builder.setCustomTitle(title);

            Spinner weekPickerSpin = (Spinner) dayOfWeekPickerView.findViewById(R.id.spinner);
            if (bDayOfWeekSet) {
              //init the spinner when the user has already set the day of week
              weekPickerSpin.setSelection(iDayOfWeek - 1);
            }
            else {
              Calendar calCurrent = Calendar.getInstance();
              //initialize it to today
              weekPickerSpin.setSelection(calCurrent.get(Calendar.DAY_OF_WEEK) - 1);
            }
            weekPickerSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
              @Override
              public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                iDayOfWeek = position + 1;
              }

              @Override
              public void onNothingSelected(AdapterView<?> parent) {

              }
            });

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                //save previous reminder date
                Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
                //get the current date to use as a base for setting next reminder
                //with the one constraint being the targetDayOfYear.
                Calendar calCurrent = Calendar.getInstance();

                int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
                int minute = prevReminderDate.get(Calendar.MINUTE);
                calNotificationDate = Calendar.getInstance();
                calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
                calNotificationDate.set(Calendar.MINUTE, minute);
                calNotificationDate.set(Calendar.DAY_OF_WEEK, iDayOfWeek);
                if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
                  calNotificationDate.add(Calendar.WEEK_OF_YEAR, 1);
                }

                bDateSet = true;
                //btn.setBackgroundResource(R.drawable.button_selected);
                //btn.setTextColor(Color.GREEN);
                bReadyToSave = true;
                invalidateOptionsMenu();
                dateTimeSettingFootnote();
                setDateTimeFootnoteText(false);
                bDayOfWeekSet = true;
              }
            });
            builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

              }
            });

            builder.setView(dayOfWeekPickerView);
            AlertDialog dlg = builder.create();
            dlg.show();
          }
          else if (primaryReminderFreq.equalsIgnoreCase("monthly")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View dayOfMonthPickerView = getLayoutInflater().inflate(R.layout.dayofmonth_chooser, null);
            builder.setTitle("Enter day of month");
            dayOfMonthEdit = (EditText) dayOfMonthPickerView.findViewById(R.id.editDayOfMonth);

            iDayOfMonth = calNotificationDate.get(Calendar.DAY_OF_MONTH);
            dayOfMonthEdit.setText("" + iDayOfMonth);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                iDayOfMonth = Integer.parseInt(dayOfMonthEdit.getText().toString());
                //save previous reminder date
                Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
                //get the current date to use as a base for setting next reminder
                //with the one constraint being the targetDayOfYear.
                Calendar calCurrent = Calendar.getInstance();

                int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
                int minute = prevReminderDate.get(Calendar.MINUTE);
                calNotificationDate = Calendar.getInstance();
                calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
                calNotificationDate.set(Calendar.MINUTE, minute);
                calNotificationDate.set(Calendar.DAY_OF_MONTH, iDayOfMonth);
                if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
                  calNotificationDate.add(Calendar.MONTH, 1);
                }
                bDayOfMonthSet = true;

                bDateSet = true;
                //btn.setBackgroundResource(R.drawable.button_selected);
                //btn.setTextColor(Color.GREEN);
                bReadyToSave = true;
                invalidateOptionsMenu();
                dateTimeSettingFootnote();
                setDateTimeFootnoteText(false);
                bDayOfMonthSet = true;
              }
            });
            builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

              }
            });
            builder.setView(dayOfMonthPickerView);
            AlertDialog dlg = builder.create();
            dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            dlg.show();
          }
          else if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
            dpd = new DatePickerDialog(ReminderScheduler.this, iThemeResourceID,
              onDateSetListener, calNotificationDate.get(Calendar.YEAR),
              calNotificationDate.get(Calendar.MONTH),
              calNotificationDate.get(Calendar.DAY_OF_MONTH));
            initDatePicker();
            dpd.show();
          }
          else if (primaryReminderFreq.equalsIgnoreCase("custom")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View customIntervalChooser = getLayoutInflater().inflate(R.layout.custom_freq_chooser, null);
            customNumET = (EditText) customIntervalChooser.findViewById(R.id.intervalET);
            customUnitSpin = (Spinner) customIntervalChooser.findViewById(R.id.unitSpin);
            if (bCustomSet) {
              customNumET.setText("" + customAmt);
              for (int i = 0; i < customUnitSpin.getCount(); i++) {
                if (customUnits.equalsIgnoreCase(customUnitSpin.getItemAtPosition(i).toString())) {
                  customUnitSpin.setSelection(i);
                }
              }
            }
            else {
              for (int i = 0; i < customUnitSpin.getCount(); i++) {
                if ("days".equalsIgnoreCase(customUnitSpin.getItemAtPosition(i).toString())) {
                  customUnitSpin.setSelection(i);
                }
              }
            }

            builder.setView(customIntervalChooser);
            //builder.setTitle("Set custom interval");

            TextView title = new TextView(this);

            // You Can Customise your Title here
            title.setText("Set custom interval");
            title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            title.setPadding(20, 20, 20, 20);
            title.setGravity(Gravity.CENTER);
            title.setTextColor(Color.WHITE);
            title.setTextSize(20);
            builder.setCustomTitle(title);


            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                customAmt = Integer.parseInt(customNumET.getText().toString());
                customUnits = customUnitSpin.getSelectedItem().toString();
                auxTimeInfo = customUnits + ":" + customAmt;
                //save previous reminder date
                Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
                //get the current date to use as a base for setting next reminder
                //with the one constraint being the targetDayOfYear.
                calNotificationDate = Calendar.getInstance();
/*
                                int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
                                int minute = prevReminderDate.get(Calendar.MINUTE);
                                int second = prevReminderDate.get(Calendar.SECOND);
                                calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
                                calNotificationDate.set(Calendar.MINUTE, minute);
                                calNotificationDate.set(Calendar.SECOND, second);
*/
                long ltime = calNotificationDate.getTimeInMillis();
                if (customUnits.equalsIgnoreCase("seconds")) {
                  //calNotificationDate.add(Calendar.SECOND, customAmt);
                  calNotificationDate.setTimeInMillis((customAmt * 1000) + ltime);
                }
                if (customUnits.equalsIgnoreCase("minutes")) {
                  //calNotificationDate.add(Calendar.MINUTE, customAmt);
                  calNotificationDate.setTimeInMillis((customAmt * 1000 * 60) + ltime);

                }
                if (customUnits.equalsIgnoreCase("hours")) {
                  //calNotificationDate.add(Calendar.HOUR_OF_DAY, customAmt);
                  calNotificationDate.setTimeInMillis((customAmt * 1000 * 60 * 60) + ltime);
                }
                if (customUnits.equalsIgnoreCase("days")) {
                  //calNotificationDate.add(Calendar.DAY_OF_YEAR, customAmt);
                  calNotificationDate.setTimeInMillis((customAmt * 1000 * 60 * 60 * 24) + ltime);
                }

                bDateSet = true;
                bCustomSet = true;
                //btn.setBackgroundResource(R.drawable.button_selected);
                //btn.setTextColor(Color.GREEN);
                bReadyToSave = true;
                invalidateOptionsMenu();
                dateTimeSettingFootnote();
              }
            });
            builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {

              }
            });

            AlertDialog dlg = builder.create();
            dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            dlg.show();
          }

        }
        break;
      }
      case MotionEvent.ACTION_UP: {
        try {
          Thread.sleep(delay);
        }
        catch (Exception e) {
        }
        v.setBackgroundColor(getResources().getColor(R.color.themeColorBackground));
        Log.d("event", "date set button up");
        break;
      }
      //need this event because it is being received when user scrolls.
      //the parent ScollView obj. the ScrollView intercepts/handles the scroll events
      //and passes the child objedts the ACTION_CANCEL
      case MotionEvent.ACTION_CANCEL: {
        try {
          Thread.sleep(delay);
        }
        catch (Exception e) {
        }
        v.setBackgroundColor(getResources().getColor(R.color.themeColorBackground));
        Log.d("event", "date set button cancel");
        break;
      }
    }
    return true;
  }

  TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
      //save previous reminder date
      Calendar prevReminderDate = (Calendar) calNotificationDate.clone();

      //use current time as base of setting new time/date
      calNotificationDate = Calendar.getInstance();
      //get the user time settings
      calNotificationDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
      calNotificationDate.set(Calendar.MINUTE, minute);
      //get the current date to use as a base for setting next reminder
      //with the one constraint being the targetDayOfYear.
      Calendar calCurrent = Calendar.getInstance();
      if (primaryReminderFreq.equalsIgnoreCase("weekly")) {
        if (bDayOfWeekSet)
          calNotificationDate.set(Calendar.DAY_OF_WEEK, iDayOfWeek);
        if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000)
          calNotificationDate.add(Calendar.WEEK_OF_YEAR, 1);
      }
      else if (primaryReminderFreq.equalsIgnoreCase("monthly")) {
        if (bDayOfMonthSet)
          calNotificationDate.set(Calendar.DAY_OF_MONTH, iDayOfMonth);

        if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000)
          calNotificationDate.add(Calendar.MONTH, 1);

      }
      else if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
        if (bDayOfYearSet)
          calNotificationDate.set(Calendar.DAY_OF_YEAR, iDayOfYear);
        if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000)
          calNotificationDate.add(Calendar.YEAR, 1);
      }
      else if (primaryReminderFreq.equalsIgnoreCase("daily") ||
        (primaryReminderFreq.equalsIgnoreCase("notify once"))) {
        if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000)
          calNotificationDate.add(Calendar.DATE, 1);
      }
      setDateTimeFootnoteText(false);
      bReadyToSave = true;
      dateTimeSettingFootnote();
      invalidateOptionsMenu();
    }
  };

  void calcReminderDateAfterFreqSwitch() {
    //save previous reminder date
    Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
    //get the current date to use as a base for setting next reminder
    //with the one constraint being the targetDayOfYear.
    Calendar calCurrent = Calendar.getInstance();

    int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
    int minute = prevReminderDate.get(Calendar.MINUTE);
    calNotificationDate = Calendar.getInstance();
    calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
    calNotificationDate.set(Calendar.MINUTE, minute);

    if (primaryReminderFreq.equalsIgnoreCase("daily") || primaryReminderFreq.equalsIgnoreCase("notify once")) {
      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
        calNotificationDate.add(Calendar.DATE, 1);
      }
      //bDayOfWeekSet = true;
    }
    else if (primaryReminderFreq.equalsIgnoreCase("weekly")) {
      int dayOfWeek = prevReminderDate.get(Calendar.DAY_OF_WEEK);
      calNotificationDate.set(Calendar.DAY_OF_WEEK, dayOfWeek);
      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
        calNotificationDate.add(Calendar.WEEK_OF_YEAR, 1);
      }
      iDayOfWeek = dayOfWeek;
      bDayOfWeekSet = true;
      bDayOfMonthSet = false;
      bDayOfYearSet = false;
    }
    else if (primaryReminderFreq.equalsIgnoreCase("monthly")) {
      int dayOfMonth = prevReminderDate.get(Calendar.DAY_OF_MONTH);
      calNotificationDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
        calNotificationDate.add(Calendar.MONTH, 1);
      }
      iDayOfMonth = dayOfMonth;
      bDayOfMonthSet = true;
      bDayOfYearSet = false;
      bDayOfWeekSet = false;
    }
    else if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
      int dayOfYear = prevReminderDate.get(Calendar.DAY_OF_YEAR);
      calNotificationDate.set(Calendar.DAY_OF_YEAR, dayOfYear);
      iDayOfYear = dayOfYear;
      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
        calNotificationDate.add(Calendar.YEAR, 1);
      }
      bDayOfYearSet = true;
      bDayOfMonthSet = false;
      bDayOfWeekSet = false;
    }

    bDateSet = true;
    //btn.setBackgroundResource(R.drawable.button_selected);
    //btn.setTextColor(Color.GREEN);
    bReadyToSave = true;
    invalidateOptionsMenu();
    dateTimeSettingFootnote();
  }


  DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
      //save previous reminder date
      Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
      //get the current date to use as a base for setting next reminder
      //with the one constraint being the targetDayOfYear.
      Calendar calCurrent = Calendar.getInstance();

      int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
      int minute = prevReminderDate.get(Calendar.MINUTE);
      calNotificationDate = Calendar.getInstance();
      calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
      calNotificationDate.set(Calendar.MINUTE, minute);
      calNotificationDate.set(Calendar.MONTH, month);
      calNotificationDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
        if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
          calNotificationDate.add(Calendar.YEAR, 1);
          bDayOfYearSet = true;
        }
        else if (primaryReminderFreq.equalsIgnoreCase("daily") ||
          (primaryReminderFreq.equalsIgnoreCase("notify once"))) {
          calNotificationDate.add(Calendar.DATE, 1);
        }
      }
      bDateSet = true;
      //btn.setBackgroundResource(R.drawable.button_selected);
      //btn.setTextColor(Color.GREEN);
      bReadyToSave = true;
      invalidateOptionsMenu();
      dateTimeSettingFootnote();
      setDateTimeFootnoteText(false);
    }
  };

  static public void showMessage(String title, String Message, Context ctx) {
    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
    builder.setCancelable(true);
    builder.setTitle(title);
    builder.setMessage(Message);
//    builder.show();
    System.out.println(Message + " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");
  }

  @Override
  protected void onResume() {
    super.onResume();
    //bInitMode = false;
  }

  @Override
  protected void onStop(){
    super.onStop();
    showMessage("Alert", "stopping ReminderScheduler activity", getApplication());
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    showMessage("Alert", "destroying ReminderScheduler activity", getApplication());
    //Toast.makeText(ReminderScheduler.this, "ending reminder schedule screen", Toast.LENGTH_SHORT).show();
  }

  @Override
  protected void onPause(){
    super.onPause();
    showMessage("Alert", "pausing ReminderScheduler activity", getApplication());
  }

  AdapterView.OnItemSelectedListener freqSpinnerItemSelectionListener =
    new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        primaryReminderFreq = primaryReminderFreqSpin.getSelectedItem().toString();
        if (position == 0) {      //once
          //setTimeDateSpinnerChoices("set time", "set date");
          //repeatOrNot.setChecked(false);
          setImageViewColor(repeatIV, R.drawable.ic_repeat, false, android.R.color.black, R.color.colorLightGray);
        }
        else if (position == 1) { //daily
          //setTimeDateSpinnerChoices("set time", "set date");
          setImageViewColor(repeatIV, R.drawable.ic_repeat, true, android.R.color.black, R.color.colorLightGray);
        }
        else if (position == 2) { // weekly
          //setTimeDateSpinnerChoices("set time", "set day of week");
          setImageViewColor(repeatIV, R.drawable.ic_repeat, true, android.R.color.black, R.color.colorLightGray);
        }
        else if (position == 3) { // monthly
          //setTimeDateSpinnerChoices("set time", "set day of month");
          setImageViewColor(repeatIV, R.drawable.ic_repeat, true, android.R.color.black, R.color.colorLightGray);
        }
        else if (position == 4) { // yearly
          //setTimeDateSpinnerChoices("set time", "set day of year");
          setImageViewColor(repeatIV, R.drawable.ic_repeat, true, android.R.color.black, R.color.colorLightGray);
        }
        else if (position == 5) { // custom
          //setTimeDateSpinnerChoices("set time", "set custom frequency");
          setImageViewColor(repeatIV, R.drawable.ic_repeat, true, android.R.color.black, R.color.colorLightGray);
        }
        calcReminderDateAfterFreqSwitch();
        setDateTimeFootnoteText(false);
        dateTimeSettingFootnote();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {

      }
    };

  static int delay = 200;

  void MySetOnTouchListener(View parentControl, final View childControlToActivate, final String mode) {
    parentControl.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
          case MotionEvent.ACTION_DOWN: {
            v.setBackgroundColor(getResources().getColor(R.color.colorVeryLightGrey));
            Log.d("ReminderScheduler", "event down");
            break;
          }
          case MotionEvent.ACTION_UP: {
            try {
              Thread.sleep(delay);
            }
            catch (Exception e) {
            }
            v.setBackgroundColor(getResources().getColor(R.color.themeColorBackground));
            if (childControlToActivate != null)
              childControlToActivate.performClick();

            Log.d("ReminderScheduler", "event up");

            if (mode.equals("time") || mode.equals("date")) {
              int iThemeResourceID = android.R.style.Theme_Holo_Light_Dialog;
              Log.d("event", "date set button down");
              //MotionEvent.ACTION_UP Won't get called until the MotionEvent.ACTION_DOWN occurred,
              // a logical explanation for this is that it's impossible for an ACTION_UP to occur
              // if an ACTION_DOWN never occurred before it.
              //This logic enables the developer to block further events after ACTION_DOWN.
              // return true if listener has consumed the event, false if you want to pass on
              // the event to other controls in the hierarchy.
              if (mode.equalsIgnoreCase("time")) {
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.N) {
                  TimePickerDialog tpd = new FixedTimePickerDlg(ReminderScheduler.this, iThemeResourceID,
                    onTimeSetListener, calNotificationDate.get(Calendar.HOUR_OF_DAY),
                    calNotificationDate.get(Calendar.MINUTE), false);
                  tpd.show();
                }
                else {
                  TimePickerDialog tpd = new TimePickerDialog(ReminderScheduler.this, iThemeResourceID,
                    onTimeSetListener, calNotificationDate.get(Calendar.HOUR_OF_DAY),
                    calNotificationDate.get(Calendar.MINUTE), false);
                  tpd.show();
                }
              }
              else if (mode.equalsIgnoreCase("date")) {
                if (primaryReminderFreq.equalsIgnoreCase("notify once") || primaryReminderFreq.equalsIgnoreCase("daily")) {
                  new DatePickerDialog(ReminderScheduler.this, iThemeResourceID,
                    onDateSetListener, calNotificationDate.get(Calendar.YEAR),
                    calNotificationDate.get(Calendar.MONTH),
                    calNotificationDate.get(Calendar.DAY_OF_MONTH)).show();
                }
                else if (primaryReminderFreq.equalsIgnoreCase("weekly")) {
                  AlertDialog.Builder builder = new AlertDialog.Builder(ReminderScheduler.this);
                  View dayOfWeekPickerView = getLayoutInflater().inflate(R.layout.dayofweek_chooser, null);
                  //builder.setTitle("Pick the day of week");
                  TextView title = new TextView(ReminderScheduler.this);

                  // You Can Customise your Title here
                  title.setText("Pick day of week");
                  title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                  title.setPadding(20, 20, 20, 20);
                  title.setGravity(Gravity.CENTER);
                  title.setTextColor(Color.WHITE);
                  title.setTextSize(20);
                  builder.setCustomTitle(title);

                  Spinner weekPickerSpin = (Spinner) dayOfWeekPickerView.findViewById(R.id.spinner);
                  if (bDayOfWeekSet) {
                    //init the spinner when the user has already set the day of week
                    weekPickerSpin.setSelection(iDayOfWeek - 1);
                  }
                  else {
                    Calendar calCurrent = Calendar.getInstance();
                    //initialize it to today
                    weekPickerSpin.setSelection(calCurrent.get(Calendar.DAY_OF_WEEK) - 1);
                  }
                  weekPickerSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                      iDayOfWeek = position + 1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                  });

                  builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      //save previous reminder date
                      Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
                      //get the current date to use as a base for setting next reminder
                      //with the one constraint being the targetDayOfYear.
                      Calendar calCurrent = Calendar.getInstance();

                      int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
                      int minute = prevReminderDate.get(Calendar.MINUTE);
                      calNotificationDate = Calendar.getInstance();
                      calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
                      calNotificationDate.set(Calendar.MINUTE, minute);
                      calNotificationDate.set(Calendar.DAY_OF_WEEK, iDayOfWeek);
                      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
                        calNotificationDate.add(Calendar.WEEK_OF_YEAR, 1);
                      }

                      bDateSet = true;
                      //btn.setBackgroundResource(R.drawable.button_selected);
                      //btn.setTextColor(Color.GREEN);
                      bReadyToSave = true;
                      invalidateOptionsMenu();
                      dateTimeSettingFootnote();
                      setDateTimeFootnoteText(false);
                      bDayOfWeekSet = true;
                    }
                  });
                  builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                  });

                  builder.setView(dayOfWeekPickerView);
                  AlertDialog dlg = builder.create();
                  dlg.show();
                }
                else if (primaryReminderFreq.equalsIgnoreCase("monthly")) {
                  AlertDialog.Builder builder = new AlertDialog.Builder(ReminderScheduler.this);
                  View dayOfMonthPickerView = getLayoutInflater().inflate(R.layout.dayofmonth_chooser, null);
                  builder.setTitle("Enter day of month");
                  dayOfMonthEdit = (EditText) dayOfMonthPickerView.findViewById(R.id.editDayOfMonth);

                  iDayOfMonth = calNotificationDate.get(Calendar.DAY_OF_MONTH);
                  dayOfMonthEdit.setText("" + iDayOfMonth);

                  builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      iDayOfMonth = Integer.parseInt(dayOfMonthEdit.getText().toString());
                      //save previous reminder date
                      Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
                      //get the current date to use as a base for setting next reminder
                      //with the one constraint being the targetDayOfYear.
                      Calendar calCurrent = Calendar.getInstance();

                      int hour = prevReminderDate.get(Calendar.HOUR_OF_DAY);
                      int minute = prevReminderDate.get(Calendar.MINUTE);
                      calNotificationDate = Calendar.getInstance();
                      calNotificationDate.set(Calendar.HOUR_OF_DAY, hour);
                      calNotificationDate.set(Calendar.MINUTE, minute);
                      calNotificationDate.set(Calendar.DAY_OF_MONTH, iDayOfMonth);
                      if ((calNotificationDate.getTimeInMillis() - calCurrent.getTimeInMillis()) < 60000) {
                        calNotificationDate.add(Calendar.MONTH, 1);
                      }
                      bDayOfMonthSet = true;

                      bDateSet = true;
                      //btn.setBackgroundResource(R.drawable.button_selected);
                      //btn.setTextColor(Color.GREEN);
                      bReadyToSave = true;
                      invalidateOptionsMenu();
                      dateTimeSettingFootnote();
                      setDateTimeFootnoteText(false);
                      bDayOfMonthSet = true;
                    }
                  });
                  builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                  });
                  builder.setView(dayOfMonthPickerView);
                  AlertDialog dlg = builder.create();
                  dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                  dlg.show();
                }
                else if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
                  dpd = new DatePickerDialog(ReminderScheduler.this, iThemeResourceID,
                    onDateSetListener, calNotificationDate.get(Calendar.YEAR),
                    calNotificationDate.get(Calendar.MONTH),
                    calNotificationDate.get(Calendar.DAY_OF_MONTH));
                  initDatePicker();
                  dpd.show();
                }
                else if (primaryReminderFreq.equalsIgnoreCase("custom")) {
                  AlertDialog.Builder builder = new AlertDialog.Builder(ReminderScheduler.this);
                  View customIntervalChooser = getLayoutInflater().inflate(R.layout.custom_freq_chooser, null);
                  customNumET = (EditText) customIntervalChooser.findViewById(R.id.intervalET);
                  customUnitSpin = (Spinner) customIntervalChooser.findViewById(R.id.unitSpin);
                  if (bCustomSet) {
                    customNumET.setText("" + customAmt);
                    for (int i = 0; i < customUnitSpin.getCount(); i++) {
                      if (customUnits.equalsIgnoreCase(customUnitSpin.getItemAtPosition(i).toString())) {
                        customUnitSpin.setSelection(i);
                      }
                    }
                  }
                  else {
                    for (int i = 0; i < customUnitSpin.getCount(); i++) {
                      if ("days".equalsIgnoreCase(customUnitSpin.getItemAtPosition(i).toString())) {
                        customUnitSpin.setSelection(i);
                      }
                    }
                  }

                  builder.setView(customIntervalChooser);
                  //builder.setTitle("Set custom interval");

                  TextView title = new TextView(ReminderScheduler.this);

                  // You Can Customise your Title here
                  title.setText("Set custom interval");
                  title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                  title.setPadding(20, 20, 20, 20);
                  title.setGravity(Gravity.CENTER);
                  title.setTextColor(Color.WHITE);
                  title.setTextSize(20);
                  builder.setCustomTitle(title);


                  builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      customAmt = Integer.parseInt(customNumET.getText().toString());
                      customUnits = customUnitSpin.getSelectedItem().toString();
                      auxTimeInfo = customUnits + ":" + customAmt;
                      //save previous reminder date
                      Calendar prevReminderDate = (Calendar) calNotificationDate.clone();
                      //get the current date to use as a base for setting next reminder
                      //with the one constraint being the targetDayOfYear.
                      calNotificationDate = Calendar.getInstance();
                      long ltime = calNotificationDate.getTimeInMillis();
                      if (customUnits.equalsIgnoreCase("seconds")) {
                        //calNotificationDate.add(Calendar.SECOND, customAmt);
                        calNotificationDate.setTimeInMillis((customAmt * 1000) + ltime);
                      }
                      if (customUnits.equalsIgnoreCase("minutes")) {
                        //calNotificationDate.add(Calendar.MINUTE, customAmt);
                        calNotificationDate.setTimeInMillis((customAmt * 1000 * 60) + ltime);

                      }
                      if (customUnits.equalsIgnoreCase("hours")) {
                        //calNotificationDate.add(Calendar.HOUR_OF_DAY, customAmt);
                        calNotificationDate.setTimeInMillis((customAmt * 1000 * 60 * 60) + ltime);
                      }
                      if (customUnits.equalsIgnoreCase("days")) {
                        //calNotificationDate.add(Calendar.DAY_OF_YEAR, customAmt);
                        calNotificationDate.setTimeInMillis((customAmt * 1000 * 60 * 60 * 24) + ltime);
                      }

                      bDateSet = true;
                      bCustomSet = true;
                      //btn.setBackgroundResource(R.drawable.button_selected);
                      //btn.setTextColor(Color.GREEN);
                      bReadyToSave = true;
                      invalidateOptionsMenu();
                      dateTimeSettingFootnote();
                    }
                  });
                  builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                  });

                  AlertDialog dlg = builder.create();
                  dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                  dlg.show();
                }
              }
            }

            break;
          }
          //need this event because it is being received when user scrolls.
          //the parent ScollView obj the ScrollView intercepts/handles the scroll events
          //and passes the child objects the ACTION_CANCEL
          case MotionEvent.ACTION_CANCEL: {
            Log.d("visualReminderLL", "event cancel");
            v.setBackgroundColor(getResources().getColor(R.color.themeColorBackground));
            break;
          }
        }
        return true;
      }
    });
  }


  private CheckBox visualNotificationCB;
  private CheckBox soundNotificationCB;
  ImageView timeImageView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.reminder_scheduler);
    auxTimeInfo = "";
    bInitMode = true;

    repeatIV = (ImageView) findViewById(R.id.repeatImageView);
    Drawable drawableRepeat = AppCompatResources.getDrawable(repeatIV.getContext(), R.drawable.ic_repeat);
    repeatIV.setImageDrawable(drawableRepeat);

    timeImageView = (ImageView) findViewById(R.id.timeImageView);
    Drawable drawableTime = AppCompatResources.getDrawable(timeImageView.getContext(), R.drawable.ic_access_time);
    timeImageView.setImageDrawable(drawableTime);


    View timeSetBtn = (View) findViewById(R.id.timeModLinearLayout);
/*
        timeSetBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return eventHandler(v, event, "time");
            }
        });
*/
    MySetOnTouchListener(timeSetBtn, null, "time");


    ImageView dateImageView = (ImageView) findViewById(R.id.dateImageView);
    Drawable drawableDate = AppCompatResources.getDrawable(dateImageView.getContext(), R.drawable.ic_date);
    dateImageView.setImageDrawable(drawableDate);
    View dateSetBtn = (View) findViewById(R.id.dateModLinearLayout);
/*
        View dateSetBtn = (View) findViewById(R.id.dateModLinearLayout);
        dateSetBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return eventHandler(v, event, "date");
            }
        });
*/
    MySetOnTouchListener(dateSetBtn, null, "date");

    reminderTitleEditText = (EditText) findViewById(R.id.reminderTitle);

    soundNotificationCB = (CheckBox) findViewById(R.id.checkSoundNotification);
    visualNotificationCB = (CheckBox) findViewById(R.id.checkVisualNotification);

    //1st get the intent data to determine where notification came from
    Bundle dataBundle = getIntent().getExtras();
    if (dataBundle != null) {
      reqID = (String) dataBundle.get(ReminderScheduler.NOTIFICATION_ID);
      notificationNum = (String) dataBundle.get(ReminderScheduler.NOTIFICATION_NUM);
      //notificationName = (String) dataBundle.get(ReminderScheduler.NOTIFICATION_NAME);
      bUpdate = true; //flag indicating that this reminder already exists and is being updated

      RemindersStorage myDb = RemindersStorage.getInstance(this);
      myDb.getAllRowData(reqID);
      Cursor res = myDb.getAllRowData(reqID);
      if (res.getCount() == 0) {
        // show message
        //showMessage("Alert","No reminders to show");
      }
      else {
        while (res.moveToNext()) {
          //get the data
          ntfDesc = res.getString(RemindersStorage.FIELDS.NAME);
          nextNotificationTimeInMillis = res.getString(RemindersStorage.FIELDS.NTFDATETIME);
          reqCode = res.getInt(RemindersStorage.FIELDS.REQCODE);
          primaryReminderFreq = res.getString(RemindersStorage.FIELDS.PRIMARYREMINDERFREQ);
          secondaryReminderFreq = res.getString(RemindersStorage.FIELDS.SECONDARYREMINDERFREQ);
          strState = res.getString(RemindersStorage.FIELDS.STATE);
          auxTimeInfo = res.getString(RemindersStorage.FIELDS.AUXTIMEINFO);
          notificationType = res.getString(RemindersStorage.FIELDS.NOTIFICATIONTYPE);
        }
        // Show all data
        // showMessage("Data", buffer.toString());
      }
      myDb.close();

      reminderTitleEditText.setText(ntfDesc);
      reminderTitleEditText.setTextColor(getResources().getColor(R.color.colorPrimaryText));
      getSupportActionBar().setTitle(R.string.mod_reminder);

      if (notificationType.equalsIgnoreCase("visual alarm")) {
        visualNotificationCB.setChecked(true);
      }
      else if (notificationType.equalsIgnoreCase("sound alarm")) {
        soundNotificationCB.setChecked(true);
      }
      else if (notificationType.equalsIgnoreCase("both")) {
        visualNotificationCB.setChecked(true);
        soundNotificationCB.setChecked(true);
      }
      bReadyToSave = true;
      bTitleSet = true;
    }
    else {
      bReadyToSave = false;
      invalidateOptionsMenu();
    }

    checkib = (ImageButton) findViewById(R.id.imgCheckOK);
    Drawable drawableCheckIM = AppCompatResources.getDrawable(checkib.getContext(), R.drawable.ic_check);
    checkib.setImageDrawable(drawableCheckIM);
    checkib.setEnabled(false);
    checkib.setVisibility(View.INVISIBLE);
    checkib.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (v != null) {
          InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        reminderTitleEditText.clearFocus();
      }
    });

    reminderTitleEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {

        EditText et = (EditText) v;
        if (hasFocus) {
          //if initializing screen and its not of existing reminder then
          //set text color to be faint. otherwise set it to be black
          if (bInitMode && !bUpdate) {
            et.setText("");
            et.setTextColor(getResources().getColor(R.color.colorPrimaryText));
            bInitMode = false;
          }
          checkib.setEnabled(true);
          checkib.setVisibility(View.VISIBLE);
        }
        else {
          if (v != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
          }
          checkib.setEnabled(false);
          checkib.setVisibility(View.INVISIBLE);
          if (et.getText().length() > 0)
            bTitleSet = true;
          else
            bTitleSet = false;
          invalidateOptionsMenu();
        }
      }
    });

    // Spinner element
    primaryReminderFreqSpin = (Spinner) findViewById(R.id.freqSpin);
    if (bUpdate) {
      for (int i = 0; i < primaryReminderFreqSpin.getCount(); i++) {
        if (primaryReminderFreq.equalsIgnoreCase(primaryReminderFreqSpin.getItemAtPosition(i).toString())) {
          primaryReminderFreqSpin.setSelection(i);
        }
      }
    }
    else {
      primaryReminderFreqSpin.setSelection(0); //set default to ONCE
      primaryReminderFreq = primaryReminderFreqSpin.getSelectedItem().toString();
    }

    primaryReminderFreqSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        primaryReminderFreqSpin.setOnItemSelectedListener(freqSpinnerItemSelectionListener);
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    //Spinner element
    secondaryReminderFreqSpin = (Spinner) findViewById(R.id.secdrFreqSpin);
    if (bUpdate) {
      for (int i = 0; i < secondaryReminderFreqSpin.getCount(); i++) {
        if (secondaryReminderFreq.equalsIgnoreCase(secondaryReminderFreqSpin.getItemAtPosition(i).toString())) {
          secondaryReminderFreqSpin.setSelection(i);
        }
      }
    }
    else {
      secondaryReminderFreqSpin.setSelection(4); //set default to hour frequency
      secondaryReminderFreq = secondaryReminderFreqSpin.getSelectedItem().toString();
    }

    calNotificationDate = Calendar.getInstance();
    if (bUpdate) {
      calNotificationDate.setTimeInMillis(Long.parseLong(nextNotificationTimeInMillis));
    }
    setDateTimeFootnoteText(true);

/*
        customNumET = (EditText) findViewById(R.id.intervalET);
        customUnitSpin = (Spinner) findViewById(R.id.unitSpin);
*/
    if (bUpdate) {
      if (primaryReminderFreq.equalsIgnoreCase("notify once")) {
        //repeatOrNot.setChecked(false);
        setImageViewColor(repeatIV, R.drawable.ic_repeat, false, android.R.color.black, R.color.colorLightGray);
      }
      else {
        if (primaryReminderFreq.equalsIgnoreCase("custom")) {
          String[] auxData = auxTimeInfo.split(":");
          customUnits = auxData[0];
          customAmt = Integer.parseInt(auxData[1]);
          bCustomSet = true;
        }
        else if (primaryReminderFreq.equalsIgnoreCase("daily")) {

        }
        else if (primaryReminderFreq.equalsIgnoreCase("weekly")) {
          bDayOfWeekSet = true;
          iDayOfWeek = calNotificationDate.get(Calendar.DAY_OF_WEEK);
        }
        else if (primaryReminderFreq.equalsIgnoreCase("monthly")) {
          bDayOfMonthSet = true;
          iDayOfMonth = calNotificationDate.get(Calendar.DAY_OF_MONTH);
        }
        else if (primaryReminderFreq.equalsIgnoreCase("yearly")) {
          bDayOfYearSet = true;
          iDayOfYear = calNotificationDate.get(Calendar.DAY_OF_YEAR);
        }

        //repeatOrNot.setChecked(true);
        setImageViewColor(repeatIV, R.drawable.ic_repeat, true, android.R.color.black, R.color.colorLightGray);
      }
    }
    else {
      //repeatOrNot.setChecked(false);
      setImageViewColor(repeatIV, R.drawable.ic_repeat, false, android.R.color.black, R.color.colorLightGray);
      primaryReminderFreqSpin.setSelection(0);
      //primaryReminderFreqSpin.setEnabled(false);
      setImageViewColor(repeatIV, R.drawable.ic_repeat, false, android.R.color.black, R.color.colorLightGray);

    }

    debugView = (TextView) findViewById(R.id.debugView);
    if (bUpdate)
      dateTimeSettingFootnote();

    View secondaryReminderLinearLayout = (View) findViewById(R.id.linearLayout2ndRmdr);
    MySetOnTouchListener(secondaryReminderLinearLayout, secondaryReminderFreqSpin, "");

    View visualReminderLinearLayout = (View) findViewById(R.id.visualNotificationTypeLL);
    MySetOnTouchListener(visualReminderLinearLayout, visualNotificationCB, "");

    View soundNtfcTypeLinearLayout = (View) findViewById(R.id.soundNotificationTypeLL);
    MySetOnTouchListener(soundNtfcTypeLinearLayout, soundNotificationCB, "");

    View linearLayoutSinCheck = (View) findViewById(R.id.linearLayoutSpinCheck);
    MySetOnTouchListener(linearLayoutSinCheck, primaryReminderFreqSpin, "");


  }


  //very important for rendering imageviews and icons correctly
  PorterDuff.Mode PDM = PorterDuff.Mode.SRC_ATOP;
  Drawable mDrawable;

  void setImageViewColor(View view, int drawableResc, boolean bEnabledCol, int enabledCol, int disabledCol) {
    if (Build.VERSION.SDK_INT < 23) {
      mDrawable = getResources().getDrawable(drawableResc);
    }
    else {
      mDrawable = getResources().getDrawable(drawableResc, getTheme());
    }
    if (bEnabledCol) {
      if (Build.VERSION.SDK_INT < 23) {
        mDrawable.setColorFilter(new PorterDuffColorFilter(getResources().getColor(enabledCol), PDM));
      }
      else {
        mDrawable.setColorFilter(new PorterDuffColorFilter(getResources().getColor(enabledCol, getTheme()), PDM));
      }
    }
    else {
      if (Build.VERSION.SDK_INT < 23) {
        mDrawable.setColorFilter(new PorterDuffColorFilter(getResources().getColor(disabledCol), PDM));
      }
      else {
        mDrawable.setColorFilter(new PorterDuffColorFilter(getResources().getColor(disabledCol, getTheme()), PDM));
      }
    }

    if (view instanceof ImageView)
      ((ImageView) view).setImageDrawable(mDrawable);
  }

  boolean isReadyToSave() {
    if (bTitleSet && bReadyToSave)
      return true;
    return false;
  }


  private void styleMenuButton(Menu menu) {
    // Find the menu item you want to style

    View view = (View) findViewById(R.id.save_reminder);

    // Cast to a TextView instance if the menu item was found
    if (view != null && view instanceof TextView) {
      setImageViewColor(view, R.drawable.ic_save, isReadyToSave(), R.color.colorWhite, R.color.colorLightGray);
      if (isReadyToSave())
      view.setEnabled(true);
      else
        view.setEnabled(false);
      MenuItem saveMI = menu.getItem(0);
      saveMI.setIcon(mDrawable);
    }
  }

  @Override
  //overriding this callback so I can dynamically enable or
  //disable the save menu button and change its color accordingly
  public boolean onPrepareOptionsMenu(Menu menu) {
    boolean result = super.onPrepareOptionsMenu(menu);

    styleMenuButton(menu);
    return result;
  }

  @Override
  //overriding this callback so I can dynamically adjust the
  //widths of the spinners so they align properly with respect
  // to each other
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    //get the spinners widths to vertically align their texts with each other
    //can't do this in onCreate() b/c the views have not been rendered yet
    // and the dimensions not known until this onWindowFocusChanged() callback
    int primW = primaryReminderFreqSpin.getWidth();
    int secndW = secondaryReminderFreqSpin.getWidth();
/*
        int thirdW = notificationTypeSpin.getWidth();
        int widthToApply = primW > secndW ? (primW > thirdW ? primW: thirdW): (secndW > thirdW ? secndW: thirdW);
*/
    int widthToApply = primW > secndW ? primW : secndW;

    primaryReminderFreqSpin.getLayoutParams().width = widthToApply;
    secondaryReminderFreqSpin.getLayoutParams().width = widthToApply;
    //notificationTypeSpin.getLayoutParams().width = widthToApply;
  }

  void setDateTimeOfCustomRepeatInterval() {
    int amount = Integer.parseInt(customNumET.getText().toString());
    String units = customUnitSpin.getSelectedItem().toString();
    auxTimeInfo = units + ":" + amount;
    if (units.equalsIgnoreCase("secs")) {
      calNotificationDate.add(Calendar.SECOND, amount);
    }
    else if (units.equalsIgnoreCase("mins")) {
      calNotificationDate.add(Calendar.MINUTE, amount);
    }
    else if (units.equalsIgnoreCase("hours")) {
      calNotificationDate.add(Calendar.HOUR_OF_DAY, amount);
    }
    else if (units.equalsIgnoreCase("days")) {
      calNotificationDate.add(Calendar.DAY_OF_YEAR, amount);
    }

  }


  final static DateFormat dateFormat = new SimpleDateFormat("hh:mm aaa z MM/dd/yyyy");
  final static DateFormat logDateFormat = new SimpleDateFormat("hh:mm:ss aaa MM/dd/yyyy");

  void dateTimeSettingFootnote() {
    //display debug info
    Calendar currentTime = Calendar.getInstance();
//        TextView debugView = (TextView) findViewById(R.id.dbgView);
    DateFormat dateDebugFormat;

    if (primaryReminderFreq.equalsIgnoreCase("custom") && (customUnits != null) &&
      (customUnits.equalsIgnoreCase("seconds") || customUnits.equalsIgnoreCase("minutes")))
      dateDebugFormat = new SimpleDateFormat("h:mm:ss aaa, E MMM d yyyy");
    else
      dateDebugFormat = new SimpleDateFormat("h:mm aaa, E MMM d yyyy");

    double diff = (double) (calNotificationDate.getTimeInMillis() - currentTime.getTimeInMillis()) / 1000;
    double numDays = (double) diff / 86400;
    double numHours = (double) diff / 3600;
    double numMin = (double) diff / 60;
    DecimalFormat numformat = new DecimalFormat("#.##");
    String debugString = "";
    String reminderFooter = "Next reminder ";

    //get the days from calendar objects, for calculating whether reminder is
    // today, tomorrow, or more days from now. this is for determining proper
    // user-friendly text to display
    int currYear = currentTime.get(Calendar.YEAR);
    int reminderYear = calNotificationDate.get(Calendar.YEAR);

    //check if years are the same
    if (currYear == reminderYear) {
      int currDayOfYear = currentTime.get(Calendar.DAY_OF_YEAR);
      int reminderDayOfYear = calNotificationDate.get(Calendar.DAY_OF_YEAR);
      //check if days are the same
      if (currDayOfYear == reminderDayOfYear) {
        // this must be today
        reminderFooter += "today, in ";
      }
      else if ((reminderDayOfYear - currDayOfYear) == 1) {
        // this must be tomorrow
        reminderFooter += "tomorrow, in ";
      }
      else
        reminderFooter += " in ";
    }
    else
      reminderFooter += " in ";

    if (numDays >= 2) {
      debugString = reminderFooter + numformat.format(numDays) + " days";
    }
    else {
      if (numHours >= 2) {
        debugString = reminderFooter + numformat.format(numHours) + " hours";
        debugString += "\nat " + dateDebugFormat.format(calNotificationDate.getTime());
      }
      else if (numMin > 0) {
        debugString = reminderFooter + numformat.format(numMin) + " minutes";
        debugString += "\nat " + dateDebugFormat.format(calNotificationDate.getTime());
      }
      else {
        debugString = "Last reminder at " + dateDebugFormat.format(calNotificationDate.getTime());
      }
    }

    setDateTimeFootnoteText(false);
    if (!bCustomSet && primaryReminderFreq.equalsIgnoreCase("custom"))
      debugView.setText("");
    else
      debugView.setText(debugString);
  }



  void updateDB(String strReqCode) {
    RemindersStorage myDb = RemindersStorage.getInstance(ReminderScheduler.this);
    SQLiteDatabase db = myDb.getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(RemindersStorage.NAME, reminderDesc); //These Fields should be your String values of actual column names
    cv.put(RemindersStorage.NTFDATETIME, "" + calNotificationDate.getTimeInMillis());
    cv.put(RemindersStorage.PRIMARYREMINDERFREQ, primaryReminderFreq);
    cv.put(RemindersStorage.SECONDARYREMINDERFREQ, secondaryReminderFreq);
    cv.put(RemindersStorage.STATE, "ON");
    cv.put(RemindersStorage.AUXTIMEINFO, auxTimeInfo);
    cv.put(RemindersStorage.NOTIFICATIONTYPE, notificationType);

    db.update(RemindersStorage.TABLE_NAME, cv, " request_code = ?", new String[]{strReqCode});
    myDb.close();
  }


  public void scheduleNotification() {

    if (calNotificationDate == null)
      return;


    int iReqCode;

    if (bUpdate)
      iReqCode = Integer.parseInt(reqID);
    else
      iReqCode = iGlobalRequestCode;

    EditText descEdit = (EditText) findViewById(R.id.reminderTitle);
    Editable edt = descEdit.getText();
    reminderDesc = edt.toString();

    //data to pass to alert handler
    Intent notifyIntent = new Intent(this, AlarmReceiver.class);
    notifyIntent.putExtra(NOTIFICATION_ID, "" + iReqCode);
    notifyIntent.putExtra(NOTIFICATION_NUM, "Primary Notification");
    notifyIntent.putExtra(NOTIFICATION_NAME, reminderDesc);

    //start scheduler
    PendingIntent broadcast = PendingIntent.getBroadcast(this, iReqCode, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

    //sets how often initial reminders are made
    primaryReminderFreq = primaryReminderFreqSpin.getSelectedItem().toString();
    secondaryReminderFreq = secondaryReminderFreqSpin.getSelectedItem().toString();
    if (soundNotificationCB.isChecked() && visualNotificationCB.isChecked())
      notificationType = "both";
    else if (soundNotificationCB.isChecked())
      notificationType = "sound alarm";
    else if (visualNotificationCB.isChecked())
      notificationType = "visual alarm";
    else
      notificationType = "passive";

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calNotificationDate.getTimeInMillis(), broadcast);
    else
      alarmManager.setExact(AlarmManager.RTC_WAKEUP, calNotificationDate.getTimeInMillis(), broadcast);

    sendDebugMsg(this, "ReminderScheduler: scheduled primary notification of \"" +
      reminderDesc + "\" for " + logDateFormat.format(calNotificationDate.getTime()) + ". request code = " + iReqCode);

    String notificationMillis = "" + calNotificationDate.getTimeInMillis();


    if (bUpdate) {
      updateDB(reqID);
    }
    else {
      //add new entry into database
      RemindersStorage myDb = RemindersStorage.getInstance(this);
      boolean status = myDb.addData(reminderDesc, iReqCode, notificationMillis, primaryReminderFreq,
        secondaryReminderFreq, auxTimeInfo, notificationType);
      if (status)
        Toast.makeText(getApplicationContext(), "Added reminder to DB", Toast.LENGTH_LONG).show();
      else
        Toast.makeText(getApplicationContext(), "Failed to add reminder to DB", Toast.LENGTH_LONG).show();
      myDb.close();
    }
    //pass back data to parent activity
    Intent resultIntent = new Intent();
    resultIntent.putExtra("request ID", new String("" + iReqCode));

    setResult(Activity.RESULT_OK, resultIntent);
    if (!bUpdate)
      iGlobalRequestCode++;

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.save_reminder) {
      scheduleNotification();
      finish();
      return true;
    }
    else if (id == R.id.cancel_reminder) {
      finish();
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.reminder_editor_menu, menu);
    return true;
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    // On selecting a spinner item
    secondaryReminderFreq = parent.getItemAtPosition(position).toString();
    // Showing selected spinner item
    Toast.makeText(parent.getContext(), "Selected: " + secondaryReminderFreq, Toast.LENGTH_LONG).show();

  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {
    Toast.makeText(parent.getContext(), "Nothing Selected: ", Toast.LENGTH_LONG).show();

  }

  //borrowed code from stackoverflow.
  // https://stackoverflow.com/questions/30789907/hide-day-month-or-year-from-datepicker-in-android-5-0-lollipop
  public void initDatePicker() {

    DatePicker dp_mes = dpd.getDatePicker();
    ;

    int year = dp_mes.getYear();
    int month = dp_mes.getMonth();
    int day = dp_mes.getDayOfMonth();

    dp_mes.init(year, month, day, new DatePicker.OnDateChangedListener() {
      @Override
      public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Log.e("selected month:", Integer.toString(monthOfYear + 1));
        //Add whatever you need to handle Date changes
      }
    });
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      int daySpinnerId = Resources.getSystem().getIdentifier("day", "id", "android");
      if (daySpinnerId != 0) {
        View daySpinner = dp_mes.findViewById(daySpinnerId);
        if (daySpinner != null) {
          daySpinner.setVisibility(View.VISIBLE);
        }
      }
      int monthSpinnerId = Resources.getSystem().getIdentifier("month", "id", "android");
      if (monthSpinnerId != 0) {
        View monthSpinner = dp_mes.findViewById(monthSpinnerId);
        if (monthSpinner != null) {
          monthSpinner.setVisibility(View.VISIBLE);
        }
      }

      int yearSpinnerId = Resources.getSystem().getIdentifier("year", "id", "android");
      if (yearSpinnerId != 0) {
        View yearSpinner = dp_mes.findViewById(yearSpinnerId);
        if (yearSpinner != null) {
          yearSpinner.setVisibility(View.GONE);
        }
      }
    }
    else { //Older SDK versions
      Field f[] = dp_mes.getClass().getDeclaredFields();
      for (Field field : f) {
        if (field.getName().equals("mDayPicker") || field.getName().equals("mDaySpinner")) {
          field.setAccessible(true);
          Object dayPicker = null;
          try {
            dayPicker = field.get(dp_mes);
          }
          catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          ((View) dayPicker).setVisibility(View.VISIBLE);
        }

        if (field.getName().equals("mMonthPicker") || field.getName().equals("mMonthSpinner")) {
          field.setAccessible(true);
          Object monthPicker = null;
          try {
            monthPicker = field.get(dp_mes);
          }
          catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          ((View) monthPicker).setVisibility(View.VISIBLE);
        }

        if (field.getName().equals("mYearPicker") || field.getName().equals("mYearSpinner")) {
          field.setAccessible(true);
          Object yearPicker = null;
          try {
            yearPicker = field.get(dp_mes);
          }
          catch (IllegalAccessException e) {
            e.printStackTrace();
          }
          ((View) yearPicker).setVisibility(View.GONE);
        }
      }
    }
  }
}
