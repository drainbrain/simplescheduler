package com.example.me.simplescheduler;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import com.example.me.simplescheduler.AlarmReceiver;
import com.example.me.simplescheduler.NotificationActivity;
import com.example.me.simplescheduler.R;
import com.example.me.simplescheduler.RemindersStorage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.example.me.simplescheduler.AlarmReceiver.sendDebugMsg;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_ID;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NAME;
import static com.example.me.simplescheduler.ReminderScheduler.NOTIFICATION_NUM;
import static com.example.me.simplescheduler.ReminderScheduler.dateFormat;
import static com.example.me.simplescheduler.RemindersStorage.STATE;

/**
 * Created by me on 5/22/2018.
 */

public class NotificationDlg extends DialogFragment {

    boolean bKeepNotifying = true;
    Calendar oldNotfTime;
    long secondsFromNow=0;
    final static String filename = "notificationHist.txt";
    static DateFormat dateLogFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm aaa z");;
    private String notificationName;
    private String reqID;
    private String notificationNum;
    private String notificationType;
    private Ringtone ring = null;
    private int iNumSecondsTillNextNotification;

    RemindersStorage myDb;
    private Calendar newNotfTime;

    public void init(String name, String num, String ID){
        notificationName = name;
        notificationNum = num;
        reqID = ID;
    }

    public void cancelNotification(){
        Intent notifyIntent = new Intent(getActivity(), AlarmReceiver.class);

        PendingIntent broadcast = PendingIntent.getBroadcast(getActivity(), Integer.parseInt(reqID), notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);

        alarmManager.cancel(broadcast);
        broadcast.cancel();
        bKeepNotifying = false;
        if(ring != null)
            ring.stop();

        //update database
        myDb = RemindersStorage.getInstance(getActivity());
        myDb.updateDBfield(STATE, "OFF", reqID);
        dismiss();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        bKeepNotifying = true;
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        getDialog().getWindow().setTitle(notificationNum);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        p.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE;
        getDialog().getWindow().setAttributes(p);

        View vw = inflater.inflate(R.layout.activity_notification, null);

        DateFormat dtf = new SimpleDateFormat("hh:mm aaa z, d MMM ''yy");
        Calendar currentTime = Calendar.getInstance();
        TextView timeStampTV = (TextView) vw.findViewById(R.id.timeStampView);
        timeStampTV.setText(dtf.format(currentTime.getTimeInMillis()));

        TextView descTV = (TextView) vw.findViewById(R.id.nameText);
        descTV.setText(notificationName);

        //determine notification type then take appropriate corresponding action
        if(notificationType.equalsIgnoreCase("sound alarm") || notificationType.equalsIgnoreCase("both")){
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
                ring = RingtoneManager.getRingtone(getActivity(), notification);
                ring.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(notificationType.equalsIgnoreCase("visual alarm") || notificationType.equalsIgnoreCase("both")){
            final Window win = getDialog().getWindow();
            win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }


        return vw;
    }

}
