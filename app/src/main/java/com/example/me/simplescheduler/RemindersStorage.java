package com.example.me.simplescheduler;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by me on 3/5/2018.
 */
class DBFields{
    public final int ID = 0;
    public final int NAME = 1;
    public final int NTFDATETIME = 2;
    public final int REQCODE = 3;
    public final int PRIMARYREMINDERFREQ = 4;
    public final int SECONDARYREMINDERFREQ = 5;
    public final int STATE = 6;
    public final int NOTIFICATIONTYPE = 7;
    public final int AUXTIMEINFO = 8;
}


public class RemindersStorage extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "Reminders.db";
    public static final String TABLE_NAME = "active_notifications";
    public static final String ID = "ID";
    public static final String NAME = "name";
    public static final String NTFDATETIME = "ntf_date_time";
    public static final String REQCODE = "request_code";
    public static final String PRIMARYREMINDERFREQ = "frequency";
    public static final String SECONDARYREMINDERFREQ = "repeat_freq";
    public static final String STATE = "state";
    public static final String NOTIFICATIONTYPE = "type";
    public static final String AUXTIMEINFO = "aux_time_info";



    public static final DBFields FIELDS = new DBFields();
    private static RemindersStorage sInstance;


    public static synchronized RemindersStorage getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new RemindersStorage(context.getApplicationContext());
        }
        return sInstance;
    }

    private RemindersStorage(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table " + TABLE_NAME +
                " (ID INTEGER PRIMARY KEY AUTOINCREMENT," + NAME + " TEXT," + NTFDATETIME + " TEXT," +
                REQCODE + " INTEGER," + PRIMARYREMINDERFREQ + " TEXT," + SECONDARYREMINDERFREQ + " TEXT,"
                + STATE + " TEXT," + NOTIFICATIONTYPE + " TEXT," + AUXTIMEINFO + " TEXT)");

        //state,request_code,ntf_date_time,aux_time_info,frequency,repeat_freq,name
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public Cursor getAllData() {
        SQLiteDatabase db = getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        return res;
    }

    public Cursor getAllRowData(String reqCode) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME + " where " + REQCODE + " = " + reqCode, null);
        return res;
    }


    public boolean addData(String desc, int reqCode, String ntfdate, String freq, String repeatFreq, String auxTimeInfo, String notificationType) {
        boolean isInserted = insertData( desc, reqCode, ntfdate, freq, repeatFreq, auxTimeInfo, notificationType);
        if(isInserted == true)
            System.out.println("Data Inserted");
        else
            System.out.println("Data Not Inserted");

        return isInserted;
    }

    public void updateDBfield(String strDBfield, String val, String strReqCode){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(strDBfield, val ); //These Fields should be your String values of actual column names
        db.update(RemindersStorage.TABLE_NAME, cv, " request_code = ?", new String[]{strReqCode});
    }

    public boolean insertData(String name, int reqCode, String ntfdate, String freq, String repeatFreq, String auxTimeInfo, String notificationType) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME,name);
        contentValues.put(NTFDATETIME, ntfdate);
        contentValues.put(REQCODE, reqCode);
        contentValues.put(PRIMARYREMINDERFREQ, freq);
        contentValues.put(SECONDARYREMINDERFREQ, repeatFreq);
        contentValues.put(STATE, "ON");
        contentValues.put(NOTIFICATIONTYPE, notificationType);
        contentValues.put(AUXTIMEINFO, auxTimeInfo);
        //state,request_code,ntf_date_time,aux_time_info,frequency,repeat_freq,name
        try {
            db.insertOrThrow(TABLE_NAME, null, contentValues);
            return true;
        }
        catch (Exception e){
            String ex = e.getMessage();
            Log.d("exception", ex);
            return false;
        }
    }

    public Integer deleteData (String id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_NAME, REQCODE + " = ?", new String[] {id});
    }

    public void deleteAllData () {
        SQLiteDatabase db = getWritableDatabase();
        //db.execSQL("delete from " + TABLE_NAME);

    }


}
