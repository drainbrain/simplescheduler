package com.example.me.simplescheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

import static com.example.me.simplescheduler.NotificationActivity.dateLogFormat;

public class AlarmReceiver extends BroadcastReceiver{


    static String fname = "/data/data/com.example.me.simplescheduler/files/notificationHist.txt";

    static void debugMesg(String strAction){

        try {
            if(!MainActivity.writeToFile)
                return;
            BufferedWriter LNFile=null;
            File logFile = new File(fname);
            if(logFile.exists()) {
                LNFile = new BufferedWriter(new FileWriter(fname, true));
            }
            else {
                LNFile = new BufferedWriter(new FileWriter(fname, false));
            }
            LNFile.write(dateLogFormat.format(new Date()) + "  " + strAction + "!\r\n");
            LNFile.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void sendDebugMsg(Context ctx, String msg){
        String deviceAttr = Build.MANUFACTURER + " " + Build.MODEL + " - SDK " + Build.VERSION.SDK_INT;
        LogSender sm = new LogSender(ctx, "lendubb@yahoo.com", deviceAttr,
                ReminderScheduler.logDateFormat.format(new Date()) + "  -  " + msg);
        Log.d("SimpleScheduler", msg);
        //Executing sendmail to send email
        sm.execute();
    }


    @Override
    public void onReceive(Context context, Intent intent) {



        Bundle dataBundle = intent.getExtras();

        if(dataBundle!=null) {
            String notificationType = (String) dataBundle.get(ReminderScheduler.NOTIFICATION_NUM);
            String notificationName = (String) dataBundle.get(ReminderScheduler.NOTIFICATION_NAME);
            String notificationID = (String) dataBundle.get(ReminderScheduler.NOTIFICATION_ID);

            //debugMesg("received alarm for \'" + notificationName + "\'");
            sendDebugMsg(context, "received alarm for " + notificationType + " of \"" + notificationName + "\"" );

            Intent notificationIntent = new Intent(context, NotificationActivity.class);
            notificationIntent.putExtra(ReminderScheduler.NOTIFICATION_NUM, notificationType);
            notificationIntent.putExtra(ReminderScheduler.NOTIFICATION_NAME, notificationName);
            notificationIntent.putExtra(ReminderScheduler.NOTIFICATION_ID, notificationID);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(notificationIntent);
        }

    }
}
