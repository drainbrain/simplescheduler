package com.example.me.simplescheduler;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
//import android.support.annotation.Nullable;
import android.app.DialogFragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;

import static com.example.me.simplescheduler.NotificationActivity.filename;
import static com.example.me.simplescheduler.R.id.debugView;

/**
 * Created by me on 4/8/2018.
 */

public class History  extends DialogFragment {
    static String getFilePath(String fname, Activity act){
        File topDir = act.getFilesDir();
        String rootDir = topDir.getAbsolutePath();
        String filePath = rootDir + "/" + filename;
        return filePath;
    }


    File logFile;
    TextView histTV;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View vw = inflater.inflate(R.layout.history, null);

        histTV = (TextView) vw.findViewById(R.id.histTV);
        histTV.setMovementMethod(new ScrollingMovementMethod());

        Button clearBtn = (Button) vw.findViewById(R.id.clearBtn);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logFile.delete();
                histTV.setText("");
            }
        });


        Button sendLogBtn = (Button) vw.findViewById(R.id.sendLogBtn);
        sendLogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceAttr = Build.MANUFACTURER + " " + Build.MODEL + " - SDK " + Build.VERSION.SDK_INT;
                //Creating SendMail object
                LogSender sm = new LogSender(getActivity(), "lendubb@yahoo.com", deviceAttr, histTV.getText().toString() );

                //Executing sendmail to send email
                sm.execute();
            }
        });


        logFile = new File(getFilePath(filename, getActivity()));
        if(logFile.exists()) {
            BufferedReader histFile = null;
            try {
                histFile = new BufferedReader(new FileReader(getFilePath(filename, getActivity())));
                String line = histFile.readLine();
                int length = 0;
                if(line != null)
                    length = line.length();
                String fileContent = "";
                boolean firsttime = true;

                while ((line != null) && (!line.isEmpty())) {
                    if(firsttime) {
                        fileContent = line;
                        firsttime = false;
                    }
                    else
                        fileContent = fileContent + "\r\n\r\n" + line;
                    line = histFile.readLine();
                    if(line != null)
                        length = length + line.length();
                }

                if(!fileContent.isEmpty())
                    histTV.setText(fileContent);
            }
            catch (Exception e){
                Log.d("exception",e.getMessage());
            }
        }
        else{
            //Toast.makeText(getDialog().getContext(), "history file " + getFilePath(filename, getActivity()) + " does not exist", Toast.LENGTH_SHORT).show();
            //MainActivity.showMessage("fuck", "history file " + getFilePath(filename, getActivity()) + " does not exist", getActivity());
        }


        return vw;
    }



}